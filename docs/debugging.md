# Debugging

## In a `file.managed`, what’s available

    {{ show_full_context() }}


## Add notes or break at state run to help understanding

Imagine you want to know what is the output of a function, you can use [`test...` state function](https://docs.saltstack.com/en/latest/ref/states/testing.html)

    {%- set noc_internal_ip = salt['mine.get']('role:noc', 'internal_ip_addrs', expr_form='grain') %}

    NOC Internal IP is:
      test.fail_without_changes:
        - name: 'Just a test  {{ noc_internal_ip|json }}'
