# Infrastructure configuration code

This repository contains configuration code to manage a cluster of servers.
It assumes you've installed the code along with its pillars both shared and private into a number
**Ubuntu 14.04 LTS** VMs, with at least one VM called "noc" (a.k.a. NOC)
which role is to orchestrate other members ("minions") around it.

This system is designed to help maintain the following aspects

* Web servers
* Web Application deployment lifecycle
* Databases
* SSL/TLS certificates
* Monitoring
* Logging
* Minimal email services
* DNS

The NOC VM holds this configuration repository (in `/srv/salt`)
and makes manipulations on other machines on its cluster.

You can run this either from your local computer using [Vagrant](https://www.vagrantup.com/),
or on a Cloud Provider (e.g. [Amazon AWS](https://aws.amazon.com/),
[Microsoft Azure](https://azure.microsoft.com), [Google GCP](https://cloud.google.com)).

The code is written for a configuration management called [Salt Stack](http://saltstack.com/).

Each deployment (e.g. on AWS for staging) has its own NOC and this should enforce a boundary between
deployment levels.

To build your own NOC, refer to [this repository](https://bitbucket.org/AliasWebServices/noc)
where it contains instructions to bring it all together.

The list of related repos is accessible in [the "**Managed Cloud**" project on BitBucket](https://bitbucket.org/account/user/AliasWebServices/projects/MC).


## Design decisions

* This repository should be exactly the same for each deployment levels
* Work should be done and tested on a local *Vagrant* cluster prior to be applied elsewhere
* What defines purpose of a VM is the name of the machine, and should be visible from the [`top.sls` file](./top.sls)
* Data to describe what gets installed is managed via **[pillars](https://bitbucket.org/AliasWebServices/pillars)**
* *Sensitive data* are stored in **[pillars-production](https://bitbucket.org/AliasWebServices/pillars-production)** and accessible only to who has access to production
* Canonical version of *Sensitive data* is stored **[pillars-all](https://bitbucket.org/AliasWebServices/pillars-production)** and should be used to develop configuration and then be merged on *pillars-production* at deployment to production time
* Packaged Web Applications is what the NOC deploys. Which is any Web based project, cloned, submodules and dependencies pulled in, package management system had their dependencies installed, the assets were minified, and then archived into a single file in `/srv/salt/files/projects/projectName-latest.tar.bz`.
* A packaged web application can be made (i.e. cloned and minified) by the NOC from `/srv/webapps/projects/projectName` but is not mandatory.
* All projects gets a self-signed SSL certificate generated,



## How to use

The following is a list of possible scenarios that may happen while maintaining a cluster.

It assumes you have a set of VMs similar to this:

* *"noc"* (NOC) where you issue all commands (e.g. `salt-call ...`)
* *"web0"* a web server where the NOC installs our packaged Web Application releases and writes current configuration files
* *"db0"* a database server. This role is only useful when the Cloud Provider such is the case when we run a cluster with Vagrant. All database server management commands such as keeping database credentials, database names, etc. are issued by the NOC

*NOTE* The role of the VM is any word before a number. "*web0*" will make the VM all states described for machines with name matching "web" as per the *top.sls* file


    ## salt/top.sls (simplified example)
    'base':
      '*':
        - ppa
        - salt
        - basesystem

      'web*':
        - stacks.apache2
        - stacks.apache2.vhosts
        - stacks.php

Since each machine gets things installed only if they match this targetting system, when we say "highstate",
Salt will apply any state described in that top file.

[Go to **usage scenarios** to continue learning *how to use*](scenarios.md)
