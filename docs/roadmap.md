# Road Map

The purpose of this document is to describe features and design decisions related to an Orchestration system to support managing deployment of Web Sites.

Marketing and Design agencies are asked to create a lot of Websites but they typically don't have a team of Engineers dedicated to system performance and reliability like Tech Startups do. Being a "sysadmin" or "DevOps" in this environment is seen simply as putting it online, and fixing it if it's broken. Generally, we know it's broken when the client calls.

That's because a lot of corners had been cut so we can make the Agency happy with deadlines.

While doing Performance and Reliability Engineering is not a core business to the Agency giving us work, making the agency NOT look amateur is important.

For that, we need to set systems in place to help us work, and gain peace of mind.

An ideal system should support:

- Maintaining Web Sites up
- Discover system issues
- Allow to roll out changes and fixes

All of which done quickly and transparently to the end user.

Any change made to a system can break stability. An innocent looking change in a loop might take more memory than before and when ran in production, would be the drop bring everything down.

There [is an other document](https://docs.google.com/a/betastream.co/document/d/1rwNO2ZpGufloqhvfVtpP-Ho8nIKLdDovcA2f2wgsZFY/edit?usp=sharing) showing what had been done so far in another document, the following will describe features we might want to have.


## Next steps


This list will help us discuss their features, their importance and which in which order we should prioritize them.


### 1. Rework release deployment

Making a release is basically pulling code from source repository, run dependency managers, and scripts to minify and do other things prior to compress to an archive.

As of what I've done so far, deploying involves doing things that would require to make the site stop working while it is deployed. It was fine so far to do the rest, but we need to improve that.

What is assumed we want is that we can replace any Web App server at any time. As transparently as we can. And also we could have any number of Web App server available for use, where projects could be deployed and activated on any of them so we can scale horizontally quickly. Logic to handle at the caching layer (e.g. Fastly) which Origin server has the site installed should be done automatically.

What OpsWorks does is to keep dated folders of past releases on target Web App server.

What the new ("deployment") system does is to take a compressed release archive, unpack it to a target Web App server ("upstream") and write the currently applicable service configuration settings. (see figure 1).

![Figure 1](assets/deployment_actions.png)
Figure 1

We could store those snapshot to S3 and we could have Jenkins make those release packages (see item 2).

Work had been done so far that it currently require to erase a directory that's serving making the site broken while it's extracting a new version.

This cannot remain like this. We knew we would have work on that, it is time.

What we should do on that work item is:

- Trigger a CI build
- Get a copy of a build artifact for deployment (so we do not need to keep all of them on any NOCs)
- Tell which build is a safe rollback point in case of need in the future
- Make NOC extract a compressed release archive on a target Web App node
- Keep record of which Web App node has which project installed (e.g. `project foo use web1` would make the project to be deployed on `web1`)
- When deploying a release, unpack the new release to any Web App node who already has a project installed (e.g. `project foo deploy` would deploy to `web1` and any other if available)
- Making a deployed release to be visible to the public a separate step from deploying (e.g. `project foo activate latest`)
- Making a rollback should be possible (e.g. `project foo activate 201608171300`)



### 2. Make CI server to create release archives and send to storage

The place where we build and store the compressed release archives is not strictly enforced because deploying is a different step than building.

Since we have an archive of releases stored outside of the Web App servers, we can bring up and tear down any node, at any time.

What we should do on that work item is:

- Support getting PHP dependencies via Composer, even from private repositories
- Build should create a compressed archive as an artifact
- Send to a storage bucket release build artifacts


### 3. Simplify launching management steps

The new system handles a lot of things in an harmonized way. Making the steps is a manual process, but now we have a system where we can capture at one place the difference between projects.

It is now time to remove manual actions to be triggered and automated

Notice that the following would require a safety lock where we should not replace things in production without explicit consent.

What we can do with what's already in place:

- Make a database dump for one project
- Make an anonymized database dump (e.g. set the same password to all users, change emails, etc.)
- Replace current database with another database dump
- Backup all media uploads
- Replace media uploads with another


### 4. Improve access control for contributors

So far, we've been asking contributors their public IP addresses so we can access some of our infrastructure components. In other case, we're sharing a username/password pair. Both of them require coordination and can be laborous.

This proposal is about setting in place a system where we issue a certificate for every contributors, the contributor can retrieve that certificate at any time and we can also revoke a certificate if need be.

This should be possible if we set in place our own PKI (Public Key Infrastructure) authority.

Another benefit of such a system is that we could have other sites we maintain to use certificates signed by that authority so we won't need to "accept" self-signed certificates for each sites.

TODO, elaborate.



### 5. Monitoring system

We want to see what's happening and their impact when things changed.

There are a many monitoring and logging aggregation solutions. What matters is to see what's happening right now and keeping a history is secondary because during an outage we'll be staring at it and we'll want to be reassured things are improving.

Proposal here is to leverage parts of the system already in place.

For instance, Apache Web server already has timers in place, it knows how long it took to serve the request, if it was a request served by a backend runtime, and how many bytes were transfered.

We could have other similar parts we could instrument and send so we can get metrics and see changes.

Each cluster (i.e. a set of VMs running on AWS main account, we may label as "DEV") already is managed by an Orchestrator ("NOC") and have one or many Web App server.  We could have a cluster to optionnaly have monitoring enabled. What that would do is to add metrics exporters to expose their data, and make one VM to scrape and graph what it got.

What we could do on this work item as a first iteration:

- Have a logging aggregator system (e.g. use LogStash and ElasticSearch to store log events)
- Make Apache send more details in its log events to aggregator
- Make Fastly send logs to aggregator
- Make a monitor dashboard with Grafana (a proposed tool) to read from ElasticSearch metrics Apache logs sent
- Tweak log filters so we keep what makes sense and strip off what's repetitive or not useful to us



### 6. Web Application node management

So far, we've been manually changing things in dashboards to have projects run on one or more servers. Many settings are involved and it's all done manually.

With the NOC, we can control any number of minions. A minion's role is defined by it's name. If we have 3 Web App server, a project gets deployed to all 3 servers.

![Figure 2](assets/multiple_hosts.png)
Figure 2

In *figure 2* above, we could see that the NOC knows of two IPs who has the "whitelabel" project. Making our caching layer to use "whitelabel.vagrant.alias.services" as origin would then always hit any nodes who has their IP as a `A` record. Adding and removing nodes would also handle the DNS record so we wouldn't need to change anything at the caching layer.

We could make a system where we can tell to deploy a project only on a subset of available servers.

Another step further could be done such that any Web application runtime we manage exposes an HTTP port in a private network ("upstream"). An application could be written in Ruby, Python, or PHP, they would all expose a Web Server, and the number of ("upstream") nodes of each type can vary. To do this, we could then have a "frontend" from which it'll always know which services are exposed by which IPs internally, and serve as proxy to caching to the public.

That "frontend" could also be a place where we could do cleaning and filtering to prevent attack or fix misbehaving code we use from our dependencies.

![Figure 3](assets/2tier.png)
Figure 3

TODO, elaborate.



### 7. One NOC to rule all the NOCs

Deploying generally involves doing things on a cluster (e.g. "DEV" environment on main AWS account), then if the other parties signs off, we'd have to repeat the tasks to other servers.

With a NOC per level, we could make another "parent" NOC where it would trigger actions
