# Usage scenarios


## Make a Web Application managed by the NOC

Use the pillars to describe the project you want to add.


1. Tell the NOC which database server host to use by default
by editing the `local:endpoints:mysql` pillar;

        ## On Vagrant noc: /srv/pillar/local/init.sls
        ## On Vagrant web-dev: /vagrant/provision/pillar.sls
        ## On Cloud-Provider noc: /etc/salt/pillar.sls
        local:
          endpoints:
            mysql: 192.168.9.1

    You can access the value using the Pillar system;

        salt-call pillar.get local:endpoints:mysql
        local:
            192.168.9.1

2. If you can control creation of databases, also add `mysql:service_account` pillar;

        ## On Vagrant noc: /srv/pillar/mysql/init.sls
        ## On Vagrant web-dev: /vagrant/provision/pillar.sls
        ## On Cloud-Provider noc: /etc/salt/pillar.sls
        mysql:
          service_account:
            username: sacct_user
            password: chgme_sacct_0e742

3. Add the project in `projects` pillar
In `pillar/projects/init.sls`, we describe where is the site document root, and
what's the name of the configuration file we want.

        ## On Vagrant noc: /srv/pillar/projects/init.sls
        ## On Vagrant web-dev: /vagrant/provision/pillar.sls
        ## On Cloud-Provider noc: /etc/salt/pillar.sls
        projects:
          foo:
            origin: git@bitbucket.org:buzz/foo-bar-bazz.git
            branch: master
            server_name: foo.com
            tls: true
            public_docroot: __www
            wordpress:
              config_filename: __wp_config/env-local.php

4. Add the project in `mysql:projects` pillar to tell which database server the deployed
site will use

    **Notice** This assumes the database server is accessible to the web server with IP `192.168.9.1`

        ## On Vagrant noc: /srv/pillar/mysql/projects.sls
        ## On Vagrant web-dev: /vagrant/provision/pillar.sls
        ## On Cloud-Provider noc: /etc/salt/pillar.sls
        mysql:
          projects:
            foo:
              ## endpoint below is not mandatory, if you do not put one, it will use
              ## local:endpoints:mysql IP by default (192.168.9.1).
              endpoint: 192.168.9.9
              username: some_user_name
              password: something_made_up

5. To see managed projects, you can ask the NOC by issuing:

    * *projects* pillar

            salt-call pillar.get projects
            local:
                ----------
                foo:
                    ----------
                    branch:
                        master
                    origin:
                        git@bitbucket.org:buzz/foo-bar-bazz.git
                    public_docroot:
                        __www
                    server_name:
                        foo.com
                    tls:
                        True
                    wordpress:
                        ----------
                        config_filename:
                            __wp_config/env-local.php

    * *mysql* pillar

            salt-call pillar.get mysql:projects
            local:
                ----------
                foo:
                    ----------
                    endpoint:
                        192.168.9.9
                    password:
                        something_made_up
                    username:
                        some_user_name




## Build and package a project

This can be done via a CI service such as Jenkins.

To skip this step, we can have all managed projects use already packaged projects in `/srv/salt/files/projects/foo-latest.tar.bz2` where "foo" matches the project key name.

If an file already exists, the noc will not overwrite it.

1. Package the project into an archive that we'll use to deploy

        project foo package


    If you set a foo project `projects:foo:tls` pillar value to a value, it should also generate self-signed
    certificates.
    The certificates will be written on the NOC in `/srv/salt/files/openssl/certificates/intermediate` with the following names:

      * `req/foo.csr` as a signing request
      * `certs/foo.pem`, and `certs/foo.crt` as certificates
      * `private/foo.insecure.key` the cert's private key


    If we need a commercial certificate later on, you could use later on use the CSR and this key.


2. Deploy a project release

        project foo deploy

    Which will take an archive on the current NOC `/srv/salt/files/projects/foo-latest.tar.bz2`,
    copy it over to a 'web*' node, extract it and write current configuration.

    Deploying a project is non-destructive, meaning that if there is already a folder on the web servers
    in `/srv/www/foo/current`, no files will be touched except the configuration.



    To make another deployment, we need to move the `/srv/www/foo/current` so we can extract another version.
    This will cause site outage, it is a know issue, but is the way it's currently implemented
    and [subject to future improvement](roadmap.md#1-rework-release-deployment).

        project foo undeploy
        project foo deploy




## Make changes to service configuration

This can happen if you want to replace dependent services such as MySQL or ElasticSearch,
and the accounts to use for a site.

This can be done by changing the pillar, then applying the change to nodes.

1. Edit the service configuration


    * Edit `mysql:projects:foo` pillar:

            ## On Vagrant noc: /srv/pillar/mysql/projects.sls
            ## On Vagrant web-dev: /vagrant/provision/pillar.sls
            ## On Cloud-Provider noc: /etc/salt/pillar.sls
            mysql:
              projects:
                foo:
                  endpoint: 192.168.9.99
                  username: newuser
                  password: newpassword

    * Review the change

            salt-call pillar.get mysql:projects:foo
            local:
                ----------
                foo:
                    ----------
                    endpoint:
                        192.168.9.99
                    password:
                        newuser
                    username:
                        newpassword


2. If you have a service account set and the depending service allows you to, make the change on the service.

        salt-call state.sls stacks.mysql.databases


3. Change the configuration for the project

        project foo configure




## Manually pull changes from Git *salt-states* and *pillars* remotes

This is done automatically when you are on a `web-dev` Vagrant VM, or on a machine
hosted on Cloud-Provider.

But if you want to do it manually because you've pushed squashed commits or
clean up the cache, you can do the following.

    salt-run fileserver.clear_cache backend=git
    salt-run fileserver.envs
    salt-run fileserver.update
    salt-run git_pillar.update

Check if you get many files listed. We should see "basesystem", "files", many more.

    salt-run fileserver.dir_list
    - .
    - basesystem
    - basesystem/files
    - basesystem/macros
    - basesystem/webapps
    - basesystem/webapps/macros
    - basesystem/webapps/macros/wordpress
    - files
    - files/openssl
    - files/openssl/certificates
    - files/openssl/certificates/certs
    - files/openssl/certificates/crl
    - files/openssl/certificates/intermediate
    - files/openssl/certificates/intermediate/certs
    - files/openssl/certificates/intermediate/crl
    - files/openssl/certificates/intermediate/newcerts
    - files/openssl/certificates/intermediate/private
    - files/openssl/certificates/intermediate/req
    - files/openssl/certificates/newcerts
    - files/openssl/certificates/private
    - files/openssl/certificates/req
    - gdnsd
    - gdnsd/files
    ...




## Create a self-signed TLS certificate with Salt

We should **ALWAYS** be using `salt-call state.sls openssl.certificates` and use those available to us
in `/srv/salt/files/openssl/certificates/intermediate` and/or add other certificates to be managed by this method.

The following was taken from [SaltStack documentation at *salt.modules.tls*](https://docs.saltstack.com/en/latest/ref/modules/all/salt.modules.tls.html).
Should be useful only for local development, NOT to be used manually for production.

    salt-call tls.create_ca alias CN='Alias Web Services CA' C=US ST=NY L=New-York O=Betastream emailAddress=certadmin@alias.services

    salt-call tls.create_csr alias CN=db1.sandbox.alias.services cert_type=client OU=Internal C=US ST=NY L=New-York O=Betastream emailAddress=certadmin@alias.services
    salt-call tls.create_ca_signed_cert alias CN=db1.sandbox.alias.services

    salt-call tls.create_csr alias CN=db2.sandbox.alias.services cert_type=server type_ext=True
    salt-call tls.create_ca_signed_cert alias CN=db2.sandbox.alias.services cert_type=server type_ext=True
    salt-call tls.create_pkcs12 alias db2.sandbox.alias.services




## Change a site release

WARNING: This process will need more work.

For now, we need to explicitly delete the web site from the server and re-deploy the package.
Planned steps should be that we have two web servers, one not exposed, and when we deploy, we
erase files from that server, deploy the release, traffic to the new server.

1. Ensure you have project you want to deploy available

    It should be in `/srv/salt/files/projects/foo-latest.tar.bz2`

        ls -1 /srv/salt/files/projects/
        foo-latest.tar.bz2
        ...

2. Check the released versions

        salt 'web*' cmd.run 'ls /srv/www/foo'
        web:
            201608151632
            201607262036
            current
        web2:
            201608151632
            201607262036
            current

2. Move `current/` release as a timestamped version

        project foo undeploy

      Which will rename `/srv/www/foo/current` to `/srv/www/foo/201608181712`

        salt 'web*' cmd.run 'ls /srv/www/foo'
        web:
            201608181712
            201608151632
            201607262036
        web2:
            201608181712
            201608151632
            201607262036

3. Deploy the new release

        project foo deploy

4. Handle file uploads

        salt web cmd.run 'cp -r /srv/www/techbetter/201608181712/public/wp-content/uploads /srv/www/techbetter/current/public/wp-content/'
        salt web cmd.run 'ls -al /srv/www/techbetter/current/public/wp-content/'
        salt web cmd.run 'chown -R www-data:www-data /srv/www/techbetter/current/public/wp-content/uploads'




## Change in a WordPress site database all users to "admin" as password

This can be useful when we want to test features on a site.

WARNING: Don't EVER do that on a production site!

If you're on a Vagrant workspace, that'll be on the "db\*" VM.

    salt db0 mysql.query foo 'UPDATE `wp_users` SET `user_pass` = "$P$BK/3Yafeg72Ziu2G72pMqtRPcZoBKj.";'

On AWS, you do through the NOC

    salt-call mysql.query foo 'UPDATE `wp_users` SET `user_pass` = "$P$BK/3Yafeg72Ziu2G72pMqtRPcZoBKj.";'

Make sure the site is activated

    salt web0 apache.a2ensite foo

To make sure nobody can access sites directly on the server but Fastly, run:

    ## Which will create /etc/apache2/fastly.conf
    ## with current Fastly server IP ranges.
    salt web0 state.sls stacks.apache2.fastly

If we also want to block the CMS "admin", we need to add them

    projects:
      foo:
        server_name: foo.org
        ## Which would create admin.foo.org as a VirtualHost
        ## In /etc/apache2/sites-available/foo.admin.conf
        admin_virtualhost: admin

Then, we can apply the state:

    salt web0 state.sls stacks.apache2.vhosts.admin
    salt web0 apache.a2ensite foo.admin




## Upgrade Salt-Stack

    salt web0 cmd.run 'rm /etc/apt/sources.list.d/saltstack-salt-trusty.list'
    salt web0 state.highstate
    salt web0 pkg.upgrade dist_upgrade=true
    salt web0 test.version




## Upgrade operating system packages

Useful for forcing a maintenance.

    salt web0 pkg.upgrade dist_upgrade=true
    salt web0 state.highstate

TODO: Document where and how security patches are already updated.


## Unpack an archive

Useful if we have an archive of files we want to send to another server.

In this example, we're sending to a server called "web5" a file containing images (`techbetter-uploads.zip`)
which, once extracted, creates a folder called "`uploads/`" we want to copy over and exist as
`/srv/www/techbetter/current/public/wp-content/uploads`.

IMPORTANT: The file `techbetter-uploads.zip` is on the NOC (one with `salt-master` running) in
`/srv/salt/files/projects/techbetter-uploads.zip`.

    salt web cp.get_file salt://files/projects/techbetter-uploads.zip /srv/www/techbetter/current/public/wp-content/uploads.zip
    salt web cmd.run 'mv /srv/www/techbetter/current/public/wp-content/uploads /srv/www/techbetter/current/public/wp-content/uploads-safe'
    salt web cmd.run 'ls /srv/www/techbetter/current/public/wp-content'
    salt web archive.cmd_unzip /srv/www/techbetter/current/public/wp-content/uploads.zip /srv/www/techbetter/current/public/wp-content
    salt web cmd.run 'ls /srv/www/techbetter/current/public/wp-content'




## Add an protected CMS admin VirtualHost

We want to expose WordPress (or other CMSes) admin only to people explicitly allowed.

There are few ways to do this, at this time we are enforcing by allowing peole through DNS records.

1. Ensure `projects:projectname:admin_virtualhost` pillar value exist.

        salt-call pillar.get projects:projectname:admin_virtualhost
        local:
           admin


2. If not, add the value `projects:projectname` pillar:

        ## On Vagrant noc: /srv/pillar/projects/init.sls
        ## On Vagrant web-dev: /vagrant/provision/pillar.sls
        ## On Cloud-Provider noc: /etc/salt/pillar.sls
        projects:
          projectname:
            ## ... other project parameters ...
            admin_virtualhost: admin
            tls: true


3. And that... #TODO

        salt \* saltutil.sync_all
        salt-call -l warning state.highstate
        cat /etc/gdnsd/zones/*
        service gdnsd restart
        salt web state.highstate
        salt web pkg.upgrade dist_upgrade=true
        salt web state.sls stacks.apache2.vhosts.admin
        salt web cmd.run 'cat /etc/apache2/sites-enabled/projectname.admin.conf'




## Backup database and uploads, send elsewhere

Assuming client made changes in dev version of the site. We want to pull uploads, and database to others.


1. Get on DEV NOC, go to the `/srv/salt/files/projects` folder

        ssh wmwp@noc.dev.azure.alias.services
        cd /srv/salt/files/projects


2. Check databases in use, create a database backup compress it

        cd /home/wmwp/
        salt-call mysql.db_list
        mysqldump prodtechbetter > techbetter-$(date +"%Y%m%d%H%M").sql
        gzip -9 techbetter-$(date +"%Y%m%d%H%M").sql


3. Backup and bring to the NOC the uploads archive

        salt web archive.tar cfz uploads.tar.gz uploads cwd=/srv/www/techbetter/current/public/wp-content
        salt web cp.push /srv/www/techbetter/current/public/wp-content/uploads.tar.gz
        mv /var/cache/salt/master/minions/web/files/srv/www/techbetter/current/public/wp-content/uploads.tar.gz techbetter-uploads.tar.gz


4. Take the uploads from where they should be replicated from

        scp wmwp@noc.prod.azure.alias.services:~/techbetter.sql.gz .
        scp wmwp@noc.prod.azure.alias.services:~/techbetter-uploads.tar.gz .


5. Pull code changes, packages

        salt-call state.sls projects.update
        webapps
        cd /srv/webapps/projects/techbetter
        cd public/wp-content/themes/walmart-tech-better
        rm -rf dist
        npm i
        bower install
        node_modules/.bin/gulp build --production
        rm -rf assets/{styles,scripts}
        mv node_modules /srv/webapps/projects/techbetter
        exit
        cd /srv/webapps/projects/techbetter

        sudo mv /srv/salt/files/projects/techbetter-latest.tar.bz2 /srv/salt/files/projects/techbetter-$(date +"%Y%m%d%H%M").tar.bz2
        salt-call state.sls projects.package

        sudo mv node_modules public/wp-content/themes/walmart-tech-better/
        webapps
        cd /srv/webapps/projects/techbetter
        git reset --hard HEAD


6. Send uploads and database dump we taken from PROD, send them to others

        scp techbetter*.sql.gz wmwp@noc.dev.azure.alias.services:~
        scp techbetter*.sql.gz wmwp@noc.uat.azure.alias.services:~
        scp techbetter-uploads.tar.gz wmwp@noc.dev.azure.alias.services:~
        scp techbetter-uploads.tar.gz wmwp@noc.uat.azure.alias.services:~


7. Get the uploads and database dump from production to dev

        scp wmwp@noc.dev.azure.alias.services:/srv/salt/files/projects/techbetter-latest.tar.bz2 .
        scp techbetter-latest.tar.bz2 wmwp@noc.prod.azure.alias.services:~/techbetter-latest.tar.bz2
        scp techbetter-latest.tar.bz2 wmwp@noc.uat.azure.alias.services:~/uattechbetter-latest.tar.bz2


8. Send the backup and database dump to another minion

        project techbetter undeploy
        project techbetter deploy
        salt web cp.get_file salt://files/projects/techbetter-uploads.tar.gz /srv/www/techbetter/current/public/wp-content/uploads.tar.gz


9. on the VM

        cd /srv/www/techbetter/current/public/wp-content/
        tar xfz uploads.tar.gz
        rm uploads.tar.gz
        chown -R www-data:www-data uploads/




## Create a database snapshot

TODO




## Change a site SSL certificate for a commercially signed

TODO




## Add another web server

TODO




## Replace a web server with another one

TODO
