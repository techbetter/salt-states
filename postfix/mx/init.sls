{%- set tld = salt['pillar.get']('local:tld', 'localhost.local') %}

include:
  - postfix

Install a Mail Exchanger:
  pkg.installed:
    - pkgs:
      - mutt
      - swaks
      - maildrop
      - pmailq
      - postfix-mysql
      - postfix-ldap

Ensure maildrop vmail user exists:
  user.present:
    - name: vmail
    - system: True
    - shell: /bin/false
    - createhome: False

Ensure maildrop vmail group exists:
  group.present:
    - name: vmail
    - system: True

/etc/postfix/Makefile:
  file.managed:
    - source: salt://postfix/files/Makefile

/etc/aliases:
  file.managed:
    - contents: |
        ## Managed by Salt Stack.
        ## See man 5 aliases for format
        ## Do not forget postalias /etc/aliases
        postmaster:    webapps
        hostmaster:    webapps
        webmaster:     webapps
  cmd.wait:
    - name: postalias /etc/aliases
    - onchanges:
      - file: /etc/aliases

## https://docs.saltstack.com/en/latest/ref/states/all/salt.states.module.html#module-salt.states.module
## https://docs.saltstack.com/en/latest/ref/modules/all/salt.modules.postfix.html
Set myorigin postfix setting to {{ salt['grains.get']('id') }}.{{ salt['grains.get']('domain') }}:
  module.run:
    - name: postfix.set_main
    - key: myorigin
    - value: '{{ salt['grains.get']('id') }}.{{ salt['grains.get']('domain') }}'

Set mydomain postfix setting to {{ salt['grains.get']('domain') }}:
  module.run:
    - name: postfix.set_main
    - key: mydomain
    - value: '{{ salt['grains.get']('domain') }}'

Set myhostname postfix setting to {{ salt['grains.get']('id') }}.{{ salt['grains.get']('domain') }}:
  module.run:
    - name: postfix.set_main
    - key: myhostname
    - value: '{{ salt['grains.get']('id') }}.{{ salt['grains.get']('domain') }}'

Set mydestination postfix setting:
  module.run:
    - name: postfix.set_main
    - key: mydestination
    - value: '$myhostname, localhost.$mydomain'

Set maildrop_destination_recipient_limit postfix setting:
  module.run:
    - name: postfix.set_main
    - key: maildrop_destination_recipient_limit
    - value: 1

Set virtual_transport postfix setting:
  module.run:
    - name: postfix.set_main
    - key: virtual_transport
    - value: maildrop

Set smtpd_banner postfix setting:
  module.run:
    - name: postfix.set_main
    - key: smtpd_banner
    - value: '$myhostname ESMTP $mail_name - Alias Web Services'

/etc/postfix/virtual_alias_maps:
  file.managed:
    - contents: |
        ## Managed by Salt Stack.
        ## This goes to virtual_alias_maps main.cf setting
        postmaster@{{ tld }} webapps
        hostmaster@{{ tld }} webapps
        postmaster@devsjr.com webapps
        hostmaster@devsjr.com webapps
        postmaster@demosjr.com webapps
        hostmaster@demosjr.com webapps
  cmd.run:
    - name: postmap /etc/postfix/virtual_alias_maps
    - onchanges:
      - file: /etc/postfix/virtual_alias_maps

Set virtual_alias_maps postfix setting:
  module.run:
    - name: postfix.set_main
    - key: virtual_alias_maps
    - value: 'hash:/etc/postfix/virtual_alias_maps'

/etc/postfix/virtual_alias_domains:
  file.managed:
    - contents: |
        ## Managed by Salt Stack.
        ## This goes to virtual_alias_domains main.cf setting
        ## Format two strings separated by spaces. It can be anything as it
        ## is not used in postfix, but it will silence possible
        ## "expected format: key whitespace value" warnings.
        ## Ref: https://blog.tinned-software.net/setup-postfix-for-multiple-domains/
        {{ tld }} #domain
        demosjr.com #domain
        devsjr.com #domain
  cmd.run:
    - name: postmap /etc/postfix/virtual_alias_domains
    - onchanges:
      - file: /etc/postfix/virtual_alias_domains

Set virtual_alias_domains postfix setting:
  module.run:
    - name: postfix.set_main
    - key: virtual_alias_domains
    - value: 'hash:/etc/postfix/virtual_alias_domains'
