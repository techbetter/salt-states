{#
 # Setup Postfix
 #
 # See other formulas:
 #  - https://github.com/tcpcloud/salt-formula-postfix
 #  - https://github.com/webplatform/salt-states/tree/201506-refactor/mail/files/mailhub/etc/postfix
 #
 # Good articles:
 #  - https://www.linux.com/learn/how-set-virtual-domains-and-virtual-users-postfix
 #}

Install Postfix:
  pkg.installed:
    - pkgs:
      - postfix
      - postfix-pcre
      - postfix-cdb

postfix:
  service.running:
    - reload: True
    - enable: True
