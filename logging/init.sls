##
## See:
## - https://docs.saltstack.com/en/latest/topics/tutorials/syslog_ng-state-usage.html
##

rsyslog:
  pkg.purged

Install syslog-ng:
  pkg.installed:
    - pkgs:
      - syslog-ng
      - syslog-ng-core

syslog_ng.write_version:
  module.run:
    - m_name: "3.6"

options.global_options:
  syslog_ng.config:
    - config:
        - time_reap: 30
        - mark_freq: 10
        - keep_hostname: "yes"
