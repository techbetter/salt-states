# -*- coding: utf-8 -*-

'''
Another slsutil module

Extending https://github.com/saltstack/salt/tree/develop/salt/modules

See also:
 - https://github.com/saltstack/salt/issues/12761
 - https://groups.google.com/forum/#!topic/salt-users/BQpzBO5RopQ

Other useful links to revisit:
  - http://stackoverflow.com/questions/20897796/using-the-output-of-salt-to-be-used-as-input-for-an-sls-state-or-pillar
  - http://stackoverflow.com/questions/21857533/defining-states-depending-on-existance-of-a-file-directory/21884669#21884669
  - https://docs.saltstack.com/en/latest/topics/grains/index.html
  - https://github.com/saltstack/salt/tree/develop/salt/grains
  - https://docs.saltstack.com/en/latest/topics/grains/index.html
  - https://docs.saltstack.com/en/latest/topics/states/index.html

'''

from string import digits

def remove_digits(string):
    return string.translate(None, digits)

def nodename_to_role(string):
    return remove_digits(string.split('-')[0])

def filter_list(list, key, value):
    '''
    Thanks to http://stackoverflow.com/questions/20529234/how-to-select-reduce-a-list-of-dictionaries-in-flask-jinja
    '''
    return filter(lambda t: t[key] == value, list)

