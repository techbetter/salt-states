# Make the following being called as the user's via SUDO
alias salt='sudo salt'
alias salt-call='sudo salt-call'
alias salt-key='sudo salt-key'
alias salt-man='echo "Reminder: this is an alias of    salt-call sys.doc" ; sudo salt-call --local sys.doc'
alias salt-events='echo "Reminder: this is an alias of    salt-run state.event pretty=True" ; sudo salt-run state.event pretty=True'

## More lazy_ shortcuts, see others at <https://github.com/renoirb/salt-basesystem/blob/master/basesystem/files/lazy_aliases.sh>
alias lazy_connections='sudo ss -lnp'

#git log 6bcb88a..HEAD --stat --pretty=short --graph
#lazy_git_changed() { echo "git log --name-status \"$@\""; read -n1 -r -p "Press space to continue..." key; git log --name-status "$@" ;}

## Screen so we see the deployment level (we will eventually use byobu)
alias scr='screen -Rd "work"'

## sudo, but keep shell environments so we can use lazy_ and other niceties.
alias besu='sudo -HE bash -l'

project() {
  echo "Reminder: This is an alias of a salt runner call     salt-run project.$2 $1"
  (sudo salt-run project.$2 $1)
}

mkcd() {
  mkdir "$1" && cd "$_"
}

## Apply highstate with two letters.
hs() {
  if [[ $1 ]]; then
    echo 'Making highstate on' $1
    echo "Reminder: This is an alias of    salt \"$1*\" state.highstate"
    sudo salt "$1*" state.highstate
  else
    echo 'Making highstate locally'
    echo "Reminder: This is an alias of    salt-call state.highstate"
    sudo salt-call state.highstate
  fi
}
