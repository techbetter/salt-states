{%- set github_secret = salt['pillar.get']('local:github_secret', 'fixme_later') %}

/etc/salt/master.d/reactors.conf:
  file.managed:
    - source: salt://salt/master/files/reactors.conf

/var/lib/salt/reactors/hook_github.sls:
  file.managed:
    - source: salt://salt/master/files/reactors/hook_github.sls.jinja
    - makedirs: True
    - template: jinja
    - context:
        github_secret: {{ github_secret }}
