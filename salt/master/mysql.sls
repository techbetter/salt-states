{%- set service_account = salt['pillar.get']('mysql:service_account', {}) %}
{%- set database_endpoint = salt['pillar.get']('local:endpoints:mysql', 'localhost') %}

include:
  - stacks.mysql

Salt Master Python MySQL bindings:
  pkg.installed:
    - name: python-mysqldb

/etc/salt/minion.d/mysql.conf:
  file.managed:
    - name: /etc/salt/minion.d/mysql.conf
    - source: salt://salt/master/files/mysql/salt_binding.conf.jinja
    - template: jinja

# Allow users on NOC to issue mysql commands
/etc/my.cnf:
  file.managed:
    - source: salt://salt/master/files/mysql/client_config.cnf.jinja
    - template: jinja
    - context:
        service_account: {{ service_account }}
        database_endpoint: {{ database_endpoint }}

Set MySQL administrator service account details to salt in /etc/salt/mysql.cnf:
  file.managed:
    - name: /etc/salt/mysql.cnf
    - source: salt://salt/master/files/mysql/client_config.cnf.jinja
    - template: jinja
    - context:
        service_account: {{ service_account }}
        database_endpoint: {{ database_endpoint }}
