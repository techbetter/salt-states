/var/lib/salt/runners:
  file.recurse:
    - makedirs: True
    - clean: True
    - exclude_pat: E@pyc$
    - source: salt://salt/master/files/runners

/etc/salt/master.d/runners.conf:
  file.managed:
    - contents: |
        runner_dirs:
          - /var/lib/salt/runners

