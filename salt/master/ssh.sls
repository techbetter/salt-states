{%- set tld = salt['pillar.get']('local:tld', 'localhost.local') %}
{%- set level = salt['grains.get']('level', 'vagrant') %}

/etc/update-motd.d/00-atop-header:
  file.managed:
    - source: salt://salt/master/files/update-motd-header.jinja
    - template: jinja
    - mode: 755
    - context:
        level: {{ level }}
        tld: {{ tld }}
