include:
  - salt
  - salt.master.mysql
  - salt.master.ssh
  - salt.bash_aliases
  - salt.master.runners
  - salt.master.reactors

Remove packages we do not need on NOC:
  pkg.purged:
    - pkgs:
      - apache2
      - apache2-bin
      - apache2-data
      - memcached

Salt Master Service:
  service.running:
    - name: salt-master
    - reload: True
    - enable: True

/etc/salt/master.d/output.conf:
  file.managed:
    - contents: |
        state_output: mixed
    - watch_in:
      - service: salt-master

/etc/salt/master.d/cp_push.conf:
  file.managed:
    - contents: |
        file_recv_size_max: 1024
        file_recv: True
    - watch_in:
      - service: salt-master

mkdocs dependencies:
  pkg.installed:
    - pkgs:
      - python-pip
  pip.installed:
    - name: mkdocs

{#
 # NOT TO FORGET
 #
 # - add webapps to sudoers
 #}

