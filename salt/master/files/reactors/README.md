# Salt Reactors

Most reactors can be defined from this salt-states repository in `_reactors/`
and beimported as `salt://_reactors/` from `/etc/salt/master.d/reactors.conf`.

But the reactors here are special kind.

They are dynamically generated so that we can generate the code with changing parameters.

That's because reactor salt system has no access to pillar data and we do not want
to hard-code secrets but rather parametrize them, and write them with current data.
