# Salt Runner folder

What's here should be imported into Salt Master VirtualBox `runner_dirs` config.

It means that what is in this folder isn't accessible via states.

The reason we put runners here is that we want them to be source-controlled, and
don't need to copy them elsewhere on the salt master.

```yaml
# e.g. /etc/salt/master.d/runners.conf
runner_dirs:
  - /var/lib/salt/runners
```

Since a runner (and reactor for that matter) doesn't have access to pillars like states,
if we need to add values dynamically, we'll use what's in `salt://salt/master/files/runners/foo.py`
and add them to this folder and make sure they aren't commited twice by adding them to `.gitignore`

To learn more about runners, refer to

* https://docs.saltstack.com/en/latest/ref/configuration/master.html#std:conf_master-runner_dirs
