#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Wrapper for allowing a project to be deployed on a node

@author Ryan Lane
@author Renoir Boulanger

See also:
 - https://docs.saltstack.com/en/latest/ref/clients/
 - http://stackoverflow.com/questions/29920575/salt-python-api-to-run-states-in-minion
 - https://github.com/saltstack/salt/tree/develop/salt/runners
 - https://www.hveem.no/salt-cli-visualization-using-runner-and-outputter
 - https://docs.saltstack.com/en/latest/ref/output/all/index.html
'''


# Import python libs
import logging
import fnmatch
import sys, yaml, json
from datetime import datetime, date, time

# Import salt libs
import salt.pillar
import salt.utils.minions
import salt.key
import salt.client
import salt.output


log = logging.getLogger(__name__)


def data(projectName=None, **kwargs):
    minion  = 'web*'
    saltenv = 'base'
    id_, grains, _ = salt.utils.minions.get_minion_data(minion, __opts__)
    if grains is None:
        grains = {'fqdn': minion}

    for key in kwargs:
        if key == 'saltenv':
            saltenv = kwargs[key]
        else:
            grains[key] = kwargs[key]

    ## We could do that, but it's inconsistent and needs more testing.
    #__opts__['pillar_source_merging_strategy'] = 'overwrite'

    pillar = salt.pillar.Pillar(
        __opts__,
        grains,
        id_,
        saltenv)
    compiled_pillar = pillar.compile_pillar()
    projects = compiled_pillar.get('projects', [])

    if projectName is None:
        result = {}
        log.error(
            'You must specify a project key',
            exc_info_on_loglevel=logging.INFO
        )
    elif projects.get(projectName, None) is None:
        result = {}
        log.error(
            'Specified project name "{0}" '
            'is not found in [{1}]'.format(projectName, ','.join(projects)),
            exc_info_on_loglevel=logging.INFO
        )
    else:
        result = projects.get(projectName)

    return result


def _report(minions, glob, cmd, stateIdentifier):
    key = salt.key.Key(__opts__)
    keys = key.list_keys()
    success = []
    nochanges = []
    didnotrun = []
    fail = {}
    for minion,ret in sorted(minions.items()):
        if ret == {}:
            nochanges.append(minion)
            continue
        vals = ret.values()[0]
        if 'result' in vals.keys():
            if vals['result']:
                success.append(minion)
            else:
                if 'comment' in vals.keys():
                    fail[minion] = vals['comment']
                else:
                    fail[minion] = vals
    for minion in sorted(set(keys['minions']) - set(minions.keys())):
        if not fnmatch.fnmatch(minion,glob):
            continue
        didnotrun.append(minion)
    print ''
    if fail:
        print 'Minions that failed:'
        print ''
        for minion,ret in fail.items():
            print "%s: %s" % (minion,ret)
        print ''
    if didnotrun:
        print 'Minions that did not run:'
        print ''
        for minion in didnotrun:
            print minion
    if success:
        print 'Minions that succeded:'
        print ''
        for minion in success:
            print minion
    if nochanges:
        print 'Minions that reported no changes:'
        print ''
        for minion in nochanges:
            print minion


def _run(glob, stateIdentifier, projectKey):
    client = salt.client.LocalClient(__opts__['conf_file'])
    client.opts['verbose'] = True
    fun = 'state.sls'
    pillarData = data(projectKey)
    print 'We are running the following "salt %s %s %s":' % (glob, fun, stateIdentifier)
    print '  project: "%s"' % (projectKey)
    print '  data: %s' % (json.dumps(pillarData))
    pillarDataString = 'pillar={"projects":{"%s":%s}}' % (projectKey, json.dumps(pillarData))
    # See https://github.com/saltstack/salt/issues/5224
    pillarOverwriteStrategy = 'localconfig=/etc/salt/pillar_overwrite.conf'
    ret = client.cmd(glob, fun, [stateIdentifier, pillarDataString, pillarOverwriteStrategy])
    minions = {}
    for minion in ret:
        minions = dict(minions.items() + ret.items())
    _report(minions, glob, fun, stateIdentifier)


def _run_no_report(fun, arg=(), tgt='role:noc'):
    client = salt.client.LocalClient(__opts__['conf_file'])
    if arg:
        output = client.cmd(tgt, fun, expr_form='grain', arg=(arg,), timeout=__opts__['timeout'])
    else:
        output = client.cmd(tgt, fun, expr_form='grain', timeout=__opts__['timeout'])

    salt.output.display_output(output, 'txt', __opts__)
    return output


def package(projectKey):
    '''
    Make sure NOC has a latest release and TLS certificates.
    '''
    glob = 'noc'
    stateIdentifier = 'projects.package,openssl.certificates'
    return _run(glob, stateIdentifier, projectKey)


def snapshot(projectKey):
    '''
    Rename a project's latest release archive to current date so we can create another packge.
    '''
    cmd = 'cmd.run'
    dateString = datetime.utcnow().strftime("%Y%m%d%H%M")
    cmdArgument = 'mv /srv/salt/files/projects/%s-latest.tar.bz2 /srv/salt/files/projects/%s-%s.tar.bz2' % (projectKey, projectKey, dateString)
    pillarData = data(projectKey)
    pillarDataString = 'pillar={"projects":{"%s":%s}}' % (projectKey, json.dumps(pillarData))
    return _run_no_report(cmd, (cmdArgument, pillarDataString), 'role:noc')


def deploy(projectKey):
    glob = 'web*'
    stateIdentifier = 'projects.deploy,openssl.deploy'
    return _run(glob, stateIdentifier, projectKey)


def undeploy(projectKey):
    cmd = 'cmd.run'
    dateString = datetime.utcnow().strftime("%Y%m%d%H%M")
    cmdArgument = 'mv /srv/www/%s/current /srv/www/%s/%s' % (projectKey, projectKey, dateString)
    pillarData = data(projectKey)
    pillarDataString = 'pillar={"projects":{"%s":%s}}' % (projectKey, json.dumps(pillarData))
    return _run_no_report(cmd, (cmdArgument, pillarDataString), 'role:web')


def configure(projectKey):
    glob = 'web*'
    stateIdentifier = 'projects.configure'
    return _run(glob, stateIdentifier, projectKey)
