include:
  - salt.master

salt-api:
  pkg.installed: []

Create self-signed TLS certificate:
  module.run:
    - name: tls.create_self_signed_cert
    - unless: test -f /etc/pki/tls/certs/localhost.crt

/etc/salt/master.d/api.conf:
  file.managed:
    - source: salt://salt/master/files/api.conf
    - watch_in:
      - service: salt-master
      - service: salt-api

Salt Api Service:
  service.running:
    - name: salt-api
    - reload: True
    - enable: True
