/etc/profile.d/C6_salt_aliases.sh:
  file.managed:
    - group: users
    - source: salt://salt/files/bash_aliases.sh

/etc/gitconfig:
  file.managed:
    - group: users
    - source: salt://salt/files/gitconfig

/etc/vimrc:
  file.managed:
    - group: users
    - source: salt://salt/files/vimrc

Packages useful when we want shell access:
  pkg.installed:
    - pkgs:
      - htop
