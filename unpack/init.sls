{%- from "basesystem/macros/unpacker.sls" import unpack_remote -%}

{% for dest,args in salt['pillar.get']('unpack', {}).items() %}

{{ unpack_remote(args.href, dest, args) }}

{% if args.APPEND_PATH is defined %}
/etc/profile.d/C6_unpacked_alias_{{ loop.index }}.sh:
  file.managed:
    - group: users
    - template: jinja
    - contents: |
        # Managed by Salt Stack.
        # Downloaded from {{ args.href }}
        export PATH="{{ args.APPEND_PATH }}:$PATH"
{% endif %}

{% endfor %}
