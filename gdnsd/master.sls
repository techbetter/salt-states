{%- set tld               = salt['pillar.get']('local:tld', 'localhost.local') %}
{%- set http_origin       = salt['pillar.get']('local:public_ips:http_origin') %}
{%- set noc_public_ip     = salt['pillar.get']('local:public_ips:noc') %}
{%- set contact_email     = salt['pillar.get']('local:contact_email', 'root@' ~ tld) %}
{%- set projects_list     = salt['pillar.get']('projects') %}
{%- set other_entries     = salt['pillar.get']('gdnsd:entries', []) %}
{%- set whitelist_entries = salt['pillar.get']('gdnsd:whitelist', []) %}

{%- set level             = salt['grains.get']('level', 'local') %}

{%- set contact_email     = contact_email|replace('@','.') %}

{%- set mine_internal_ips = salt['mine.get']('*', 'internal_ip_addrs').items() %}

{#
 # Also, if it fails, look to ensure mine has sent its data
 #
 #     salt '*' mine.send network.interfaces
 #
 # The following should not have empty data
 #
 #     salt '*' mine.get '*' network.interfaces
 #     salt '*' mine.get '*' internal_ip_addrs
 #
 # Or from the salt-master
 #
 #     salt-run mine.get '*' internal_ip_addrs
 #
 #}

include:
  - gdnsd

{%- if salt['grains.get']('role', 'Not a role') == 'noc' %}

{%-   if salt['grains.get']('master_public_ip4') %}
{%-     set noc_public_ip = salt['grains.get']('master_public_ip4') %}
{%-   endif %}

{%-   if projects_list is defined %}

Ensure Python Timelib is installed for gdnsd.master state to work:
  pkg.installed:
    - name: python-timelib

{#      The following block requires salt://_modules/slsutils.py  #}
{%-     set all_internal_ip_addrs = [] %}
{%-     for name,private_ip in mine_internal_ips %}
{%-       set role = salt['slsutils.nodename_to_role'](name) %}
{%-       set row = {'name': name, 'private_ip': private_ip[0], 'role': role} %}
{%-       do all_internal_ip_addrs.append(row) %}
{%-     endfor %}

{%-     set noc_data = salt['slsutils.filter_list'](all_internal_ip_addrs, 'role', 'noc') %}
{%-     if noc_data|count() != 1 %}
{%-       set noc_name = 'noc' %}
{%-       set noc_private_ip = False %}
Cannot apply current state:
  test.fail_without_changes:
    - name: 'There are no minions that has their name starting by "noc"'
{%-     else %}
{%-       set noc_name = noc_data[0].get('name') %}
{%-       set noc_private_ip = noc_data[0].get('private_ip') %}
{%-     endif %}

{#
 # DNS "glue" record to state that this NOC is its own Name server
 #
 # In other words, need those two same entries at both the DNS provider dashboard
 # (e.g. CloudFlare DNS settings), and the zone file (defined in the state below).
 #
 # ```
 # prod.example.org.                 IN NS   noc.prod.example.org.
 # noc.prod.example.org.             IN A    192.0.2.1
 # ```
 #
 # Ref:
 #  - http://www.zytrax.com/books/dns/ch8/ns.html
 #  - http://unix.stackexchange.com/questions/115924/zone-delegation-in-bind
 #}
/etc/gdnsd/zones/{{ level }}.{{ tld }}:
  file.managed:
    - source: salt://gdnsd/files/zonefile.conf.jinja
    - template: jinja
    - watch_in:
      - service: gdnsd
    - context:
        tld: {{ tld }}
        level: {{ level }}
        ## This line below is why we need python-timelib.
        ## It makes an new date based timestamp increment automatically.
        timestamp: {{ "now"|strftime("%Y%m%d%H%M") }}
        projects_list: {{ projects_list }}
        http_origin: {{ http_origin }}
        other_entries: {{ other_entries }}
        whitelist_entries: {{ whitelist_entries }}
        all_internal_ip_addrs: {{ all_internal_ip_addrs }}
        contact_email_zone_format:  {{ contact_email }}
        noc_name: {{ noc_name }}
        noc_public_ip: {{ noc_public_ip }}
        noc_private_ip: {{ noc_private_ip }}

{%-   else %}

Ensure we apply GDNSD zonefile only when we have projects list:
  test.fail_without_changes:
    - name: We did not find any projects pillar

{%-   endif %}{#- End of if projects_list is defined #}

{%- else %}{#- Else of If salt['grains.get']('role') == 'noc'  #}

Ensure we apply GDNSD as master only to salt master:
  test.fail_without_changes:
    - name: 'This minion role invalid, currently defined as: {{ salt['grains.get']('role', 'Not a role') }}'

{%- endif %}{#- End of salt['grains.get']('role') == 'noc' #}
