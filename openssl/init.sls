{%- set pki_subj_o = salt['pillar.get']('openssl:contact_details:organization', 'Snake Oil Ltd.') -%}
{%- set pki_subj_ou = salt['pillar.get']('openssl:contact_details:organization_unit', 'IT Department') -%}
{%- set pki_subj_email = salt['pillar.get']('openssl:contact_details:contact_email', 'ca@example.org') -%}
{%- set level = salt['grains.get']('level', 'local') -%}

{%- set root_ca_path = '/srv/salt/files/openssl/certificates' %}

Install OpenSSL:
  pkg.installed:
    - name: openssl

{{ root_ca_path }}/openssl.cnf:
  file.managed:
    - source: salt://openssl/files/ca-root.cnf.jinja
    - template: jinja
    - makedirs: True
    - context:
        pki_subj_o: {{ pki_subj_o }}
        pki_subj_ou: {{ pki_subj_ou }}
        pki_subj_email: {{ pki_subj_email }}
        level: {{ level }}

{{ root_ca_path }}/intermediate/openssl.cnf:
  file.managed:
    - source: salt://openssl/files/ca-intermediate.cnf.jinja
    - template: jinja
    - makedirs: True
    - context:
        pki_subj_o: {{ pki_subj_o }}
        pki_subj_ou: {{ pki_subj_ou }}
        pki_subj_email: {{ pki_subj_email }}
        level: {{ level }}

##
## Test using OpenSSL testing server
##
# openssl verify -CAfile /etc/ssl/ca/certs/ca.pem /etc/ssl/ca/certs/walmart-csb.pem
# openssl s_server -cert /etc/ssl/ca/certs/walmart-csb.pem -www -accept 4567
# openssl s_client -connect localhost:4567
