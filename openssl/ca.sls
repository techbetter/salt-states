include:
  - openssl

{%- set ca_secret = salt['pillar.get']('openssl:ca_secret', 'DefaultCaSecretString') -%}
{%- set root_ca_path = '/srv/salt/files/openssl/certificates' %}

Ensure we have a Root CA:
  cmd.script:
    - cwd: {{ root_ca_path }}
    - unless: test -f {{ root_ca_path }}/private/ca.key
    - source: salt://openssl/scripts/ca-root.sh
    - env:
      - CA_SECRET: {{ ca_secret }}
      - CA_PATH: {{ root_ca_path }}
