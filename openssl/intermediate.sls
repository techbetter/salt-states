include:
  - openssl

{%- set ca_secret = salt['pillar.get']('openssl:ca_secret', 'DefaultCaSecretString') -%}
{%- set intermediate_ca_secret = salt['pillar.get']('openssl:intermediate_ca_secret', 'DefaultIntermediateCaSecretString') -%}
{%- set root_ca_path = '/srv/salt/files/openssl/certificates' %}

Ensure we have an Intermediate Root CA:
  cmd.script:
    - cwd: {{ root_ca_path }}
    - unless: test -f {{ root_ca_path }}/intermediate/private/intermediate.key
    - source: salt://openssl/scripts/ca-intermediate.sh
    - env:
      - CA_SECRET: {{ ca_secret }}
      - INTERMEDIATE1_CA_SECRET: {{ intermediate_ca_secret }}
      - CA_PATH: {{ root_ca_path }}
