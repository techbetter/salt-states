include:
  - stacks.apache2.vhosts

{%- set apache_tls_certs_path = '/etc/apache2/tls' %}

{%- for slug,obj in salt['pillar.get']('projects', {}).items() %}

{%-   set tls = obj.get('tls', None) %}

{%-   if tls %}
Deploy {{ apache_tls_certs_path }}/{{ slug }}.pem certificate:
  file.managed:
    - source: salt://files/openssl/certificates/intermediate/certs/{{ slug }}.pem
    - name: {{ apache_tls_certs_path }}/{{ slug }}.pem
    - makedirs: True
    ## Reload apache2 webserver if we changed SSL certificates
    - watch_in:
      - service: Apache Web Server

Deploy {{ apache_tls_certs_path }}/{{ slug }}.insecure.key:
  file.managed:
    - source: salt://files/openssl/certificates/intermediate/private/{{ slug }}.insecure.key
    - name: {{ apache_tls_certs_path }}/{{ slug }}.insecure.key
    - makedirs: True
    ## Reload apache2 webserver if we changed SSL certificates
    - watch_in:
      - service: Apache Web Server

Deploy {{ apache_tls_certs_path }}/{{ slug }}.crt:
  file.managed:
    - source: salt://files/openssl/certificates/intermediate/certs/{{ slug }}.crt
    - name: {{ apache_tls_certs_path }}/{{ slug }}.crt
    - makedirs: True
    ## Reload apache2 webserver if we changed SSL certificates
    - watch_in:
      - service: Apache Web Server
{%-  endif %}
{%- endfor %}

Deploy {{ apache_tls_certs_path }}/ca-chain.pem Root Chain certificate:
  file.managed:
    - source: salt://files/openssl/certificates/intermediate/certs/ca-chain.pem
    - name: {{ apache_tls_certs_path }}/ca-chain.pem
    - makedirs: True
    ## Reload apache2 webserver if we changed SSL certificates
    - watch_in:
      - service: Apache Web Server
