{%- if salt['grains.get']('id') in ['noc', 'web-dev'] %}

include:
  - openssl.ca
  - openssl.intermediate

{% set intermediate_ca_secret = salt['pillar.get']('openssl:intermediate_ca_secret', 'DefaultIntermediateCaSecretString') -%}

{%- set root_ca_path = '/srv/salt/files/openssl/certificates' -%}

{%- set tld = salt['pillar.get']('local:tld', 'localhost.local') -%}
{%- set level = salt['grains.get']('level', 'local') -%}

{% for slug,obj in salt['pillar.get']('projects', {}).items() %}

{%- set server_name = obj.get('server_name', None) %}
{%- set tls = obj.get('tls', None) %}
{%- set admin_virtualhost = obj.get('admin_virtualhost', '') %}

{%- set fqdn = slug ~ '.' ~ level ~'.' ~ tld %}

{%- if tls %}
{%-   if server_name %}
{%-     if server_name == '.' %}
{%-       set server_name = '%s.%s.%s'|format(slug, level, tld) %}
{%-     endif %}
Create SSL Certificate pack files for {{ slug }}:
  cmd.script:
    - cwd: {{ root_ca_path }}
    - unless: test -f {{ root_ca_path }}/intermediate/certs/{{ slug }}.pem
    - source: salt://openssl/scripts/ca-cert-server.sh
    - env:
      - SLUG: {{ slug }}
      - CA_PATH: {{ root_ca_path }}
      - SUBJ_CN: {{ fqdn }}
      - LEVEL: {{ level }}
      - TLD: {{ tld }}
      - SERVER_NAME_SAN: {{ server_name }}
      - INTERMEDIATE1_CA_SECRET: {{ intermediate_ca_secret }}
      - ADMIN_VIRTUALHOST: {{ admin_virtualhost }}
{%-   endif %}
{%- endif %}

{% endfor %}

Create SSL Certificate Signing Request for {{ level }}:
  cmd.script:
    - cwd: {{ root_ca_path }}
    - unless: test -f {{ root_ca_path }}/intermediate/req/{{ level }}.csr
    - source: salt://openssl/scripts/ca-cert-server.sh
    - env:
      - SLUG: {{ level }}
      - CA_PATH: {{ root_ca_path }}
      - SUBJ_CN: {{ level }}.{{ tld }}
      - LEVEL: {{ level }}
      - TLD: {{ tld }}
      - INTERMEDIATE1_CA_SECRET: {{ intermediate_ca_secret }}
      - CSR_ONLY: "1"

{%- else %}{#- End If 'noc' in salt['grains.get']('id') #}

Ensure we run NOC specific LibreSSL TLS certificate generation only where it should:
  test.fail_without_changes:
    - name: This state is not meant to be run on other node type than the NOC or web-dev

{%- endif %}{#- End Else of If 'noc' in salt['grains.get']('id') #}
