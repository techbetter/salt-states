#!/bin/bash

set -e


## Add optional shell options
for i in "$@"
do
case $i in
    --show-details)
    SHOW_DETAILS=1
    shift # past argument with no value
    ;;
    --show-commands)
    SHOW_COMMANDS=1
    shift # past argument with no value
    ;;
    *)
    # unknown option
    ;;
esac
done


if [[ -z "${CA_PATH}" ]]; then
  CA_PATH="."
fi

if [[ -z "${CA_SECRET}" ]]; then
  # export is required here because we
  # will use openssl -passin/-passout options
  # and this needs to be exported
  export CA_SECRET="casecret"
fi

if [[ -f "${CA_PATH}/intermediate/certs/intermediate.crt" ]]; then
  echo 'We already have an intermediate CA, we will not make another parent Root CA'
  exit 1
fi



declare -A paths
paths['key']="${CA_PATH}/private/ca.key"
paths['cert']="${CA_PATH}/certs/ca.crt"
paths['serial']="${CA_PATH}/serial"

declare -A human_explanation
human_explanation['key']="Root Certificate private key, passphrase is ${CA_SECRET}"
human_explanation['cert']="Root Certificate. Once we have an intermediate certificate, we should hide that one"
human_explanation['serial']="An incremental serial number to manage certificate inventory"

declare -A file_modes
file_modes['key']="400"
file_modes['cert']="444"

# Add to this array what gotten created
declare -a created



mkdir -p ${CA_PATH}/certs

chmod 700 ${CA_PATH}/certs

mkdir -p ${CA_PATH}/newcerts

chmod 700 ${CA_PATH}/newcerts

mkdir -p ${CA_PATH}/crl

mkdir -p ${CA_PATH}/req

mkdir -p ${CA_PATH}/private

touch ${CA_PATH}/index.txt

[[ -f ${CA_PATH}/serial ]] || echo 1000 > ${CA_PATH}/serial



GENRSA="\
openssl genrsa -aes256 \
               -passout env:CA_SECRET \
               -out ${paths['key']} 4096"

if [[ ${SHOW_COMMANDS} == "1" ]]; then
  echo ${GENRSA}
fi

if [[ ! -f ${paths['key']} ]]; then
  ${GENRSA} && \
  created=(${created[@]} 'key')
fi



ROOT_CA_CERT="\
openssl req -new -sha256 -batch \
            -passin env:CA_SECRET \
            -config ${CA_PATH}/openssl.cnf \
            -x509 -days 7300 -extensions v3_ca \
            -key ${paths['key']} \
            -out ${paths['cert']}"

if [[ ${SHOW_COMMANDS} == "1" ]]; then
  echo ${ROOT_CA_CERT}
fi

if [[ ! -f ${paths['cert']} ]]; then
  (${ROOT_CA_CERT}) && \
  created=(${created[@]} 'cert')
fi



REVIEW_CERT="openssl x509 -noout -text -in ${paths['cert']}"

if [[ ${SHOW_COMMANDS} == "1" ]]; then
  echo ${REVIEW_CERT}
fi

if [[ ! -f ${paths['cert']}.txt ]]; then
  ${REVIEW_CERT} > ${paths['cert']}.txt
fi



if [[ ${SHOW_COMMANDS} == "1" ]]; then
  if [[ ${#created[@]} -gt 0 ]]; then
    echo "New files created:"
    for k in ${created[@]}; do
      echo "  - ${paths[${k}]}: ${human_explanation[${k}]}"
    done
  else
    echo 'No files created'
  fi
fi
