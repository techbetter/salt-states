#!/bin/bash

set -e


## Add optional shell options
for i in "$@"
do
case $i in
    --show-details)
    SHOW_DETAILS=1
    shift # past argument with no value
    ;;
    --show-commands)
    SHOW_COMMANDS=1
    shift # past argument with no value
    ;;
    *)
    # unknown option
    ;;
esac
done


if [[ -z "${CA_PATH}" ]]; then
  CA_PATH="."
fi

if [[ -z "${CA_SECRET}" ]]; then
  # export is required here because we
  # will use openssl -passin/-passout options
  # and this needs to be exported
  export CA_SECRET="casecret"
fi

if [[ -z "${INTERMEDIATE1_CA_SECRET}" ]]; then
  # export is required here because we
  # will use openssl -passin/-passout options
  # and this needs to be exported
  export INTERMEDIATE1_CA_SECRET="intermediatesecret"
fi



declare -A paths
paths['key']="${CA_PATH}/intermediate/private/intermediate.key"
paths['cert']="${CA_PATH}/intermediate/certs/intermediate.crt"
paths['req']="${CA_PATH}/intermediate/req/intermediate.csr"
paths['serial']="${CA_PATH}/intermediate/serial"
paths['ca_chain']="${CA_PATH}/intermediate/certs/ca-chain.pem"
paths['ca_chain_parent']="${CA_PATH}/intermediate/certs/ca-chain-parent.pem"

declare -A human_explanation
human_explanation['key']="Intermediate Root Certificate private key, passphrase is ${INTERMEDIATE1_CA_SECRET}"
human_explanation['cert']="Intermediate Root Certificate. Once we have an intermediate certificate, we should hide that one"
human_explanation['req']="Intermediate CA request for ${SUBJ_CN}. We will sign this one with our own Root CA"
human_explanation['serial']="An incremental serial number to manage certificate inventory"
human_explanation['ca_chain']="Certificate authority trust chain file"
human_explanation['ca_chain_parent']="Certificate authority trust chain file parent file to prepend with"

declare -A file_modes
file_modes['key']="400"
file_modes['cert']="444"
file_modes['ca_chain']="444"
file_modes['ca_chain_parent']="444"

# Add to this array what gotten created
declare -a created



mkdir -p ${CA_PATH}/intermediate/certs

chmod 700 ${CA_PATH}/intermediate/certs

mkdir -p ${CA_PATH}/intermediate/newcerts

chmod 700 ${CA_PATH}/intermediate/newcerts

mkdir -p ${CA_PATH}/intermediate/crl

mkdir -p ${CA_PATH}/intermediate/req

mkdir -p ${CA_PATH}/intermediate/private

touch ${CA_PATH}/intermediate/index.txt

[[ -f ${CA_PATH}/intermediate/serial ]] || echo 1000 > ${CA_PATH}/intermediate/serial



GENRSA="\
openssl genrsa -aes256 \
               -passout env:INTERMEDIATE1_CA_SECRET \
               -out ${paths['key']} 4096"

if [[ ${SHOW_COMMANDS} == "1" ]]; then
  echo ${GENRSA}
fi

if [[ ! -f ${paths['key']} ]]; then
  ${GENRSA} && \
  created=(${created[@]} 'key')
fi



INTERMEDIATE1_ROOT_CSR="\
openssl req -new -sha256 -batch \
            -passin env:INTERMEDIATE1_CA_SECRET \
            -config ${CA_PATH}/intermediate/openssl.cnf \
            -key ${paths['key']} \
            -out ${paths['req']}"

if [[ ${SHOW_COMMANDS} == "1" ]]; then
  echo ${INTERMEDIATE1_ROOT_CSR}
fi

if [[ ! -f ${paths['req']} ]]; then
  (${INTERMEDIATE1_ROOT_CSR}) && \
  created=(${created[@]} 'req')
fi



REVIEW_INTERMEDIATE1_CSR="openssl req -noout -text -in ${paths['req']}"

if [[ ${SHOW_COMMANDS} == "1" ]]; then
  echo ${REVIEW_INTERMEDIATE1_CSR}
fi

if [[ ! -f ${paths['req']}.txt ]]; then
  ${REVIEW_INTERMEDIATE1_CSR} > ${paths['req']}.txt
fi




SIGN_INTERMEDIATE1_CERTIFICATE="\
openssl ca -config ${CA_PATH}/openssl.cnf -batch \
           -passin env:CA_SECRET \
           -extensions v3_intermediate_ca \
           -days 3650 -notext -md sha256 \
           -in ${paths['req']} \
           -out ${paths['cert']}"

if [[ ${SHOW_DETAILS} == "1" ]]; then
  echo ${SIGN_INTERMEDIATE1_CERTIFICATE}
fi

if [[ ! -f ${paths['cert']} ]]; then
  ${SIGN_INTERMEDIATE1_CERTIFICATE} && \
  created=(${created[@]} 'cert')
fi



if [[ ! -f ${paths['ca_chain']} ]]; then
  if [[ -f ${paths['ca_chain_parent']} ]]; then
    cat ${paths['ca_chain_parent']} > ${paths['ca_chain']} && \
    created=(${created[@]} 'ca_chain_parent')
  fi
  cat ${paths['cert']} certs/ca.crt >> ${paths['ca_chain']} && \
  created=(${created[@]} 'ca_chain')
fi



REVIEW_CERT="openssl x509 -noout -text -in ${paths['cert']}"

if [[ ${SHOW_COMMANDS} == "1" ]]; then
  echo ${REVIEW_CERT}
fi

if [[ ! -f ${paths['cert']}.txt ]]; then
  ${REVIEW_CERT} > ${paths['cert']}.txt
fi



if [[ ${SHOW_COMMANDS} == "1" ]]; then
  if [[ ${#created[@]} -gt 0 ]]; then
    echo "New files created:"
    for k in ${created[@]}; do
      echo "  - ${paths[${k}]}: ${human_explanation[${k}]}"
    done
  else
    echo 'No files created'
  fi
fi
