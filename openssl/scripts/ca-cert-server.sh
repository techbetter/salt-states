#!/bin/bash

set -e


## Add optional shell options
for i in "$@"
do
case $i in
    --show-details)
    SHOW_DETAILS=1
    shift # past argument with no value
    ;;
    --show-commands)
    SHOW_COMMANDS=1
    shift # past argument with no value
    ;;
    --csr-only)
    CSR_ONLY=1
    shift # past argument with no value
    ;;
    *)
    # unknown option
    ;;
esac
done


if [[ -z "${CA_PATH}" ]]; then
  CA_PATH="."
fi

if [[ -z "${KEY_SECRET}" ]]; then
  # export is required here because we
  # will use openssl -passin/-passout options
  # and this needs to be exported
  export KEY_SECRET="certsecret"
fi

if [[ -z "${INTERMEDIATE1_CA_SECRET}" ]]; then
  # export is required here because we
  # will use openssl -passin/-passout options
  # and this needs to be exported
  export INTERMEDIATE1_CA_SECRET="intermediatesecret"
fi

if [[ -z "${SLUG}" ]]; then
  SLUG="foo"
fi

if [[ -z "${LEVEL}" ]]; then
  LEVEL="sandbox"
fi

if [[ -z "${TLD}" ]]; then
  TLD="alias.services"
fi

if [[ -z "${SUBJ_CN}" ]]; then
  # Only domain names
  SUBJ_CN="${SLUG}.${LEVEL}.${TLD}"
fi

if [[ -z "${SUBJ_EMAIL}" ]]; then
  # Only valid email
  SUBJ_EMAIL="root@${SUBJ_CN}"
fi

if [[ -z "${SUBJ_OU}" ]]; then
  # Please no spaces!
  SUBJ_OU="Betastream"
fi

if [[ -z "${SUBJ_O}" ]]; then
  # Please no spaces!
  SUBJ_O="GroupSJR"
fi



if [[ ! -f "${CA_PATH}/intermediate/private/intermediate.key" ]]; then
  echo 'Please make sure you have an Intermediate Root CA certificate!'
  exit 1
fi



SUBJ_LINE=$(echo "/C=US/O=${SUBJ_O}/OU=${SUBJ_OU}/CN=${SUBJ_CN}" | sed 's/\s/+/g')

if [[ ${SHOW_DETAILS} == "1" ]]; then
  echo ${SUBJ_LINE}
fi



##
## Create SSL certificate alternate names
## See:
##  - http://apetec.com/support/GenerateSAN-CSR.htm
##  - http://security.stackexchange.com/questions/74345/provide-subjectaltname-to-openssl-directly-on-command-line#91556
SUBJ_SAN="\nsubjectAltName=DNS:${SUBJ_CN}"
SUBJ_SAN+=",DNS:${SLUG}.devsjr.com"
SUBJ_SAN+=",DNS:${SLUG}.demosjr.com"
SUBJ_SAN+=",DNS:${SLUG}.prodsjr.com"
if [[ ! -z "${SERVER_NAME_SAN}" ]]; then
  SUBJ_SAN+=",DNS:${SERVER_NAME_SAN}"
fi
if [[ ! -z "${SERVER_NAME_SAN}" ]]; then
  SUBJ_SAN+=",DNS:www.${SERVER_NAME_SAN}"
fi
if [[ ${ADMIN_VIRTUALHOST} ]]; then
  SUBJ_SAN+=",DNS:${ADMIN_VIRTUALHOST}.${SLUG}.${LEVEL}.${TLD}"
  SUBJ_SAN+=",DNS:${ADMIN_VIRTUALHOST}.${SERVER_NAME_SAN}"
fi
if [[ "${SLUG}.${LEVEL}.${TLD}" != "${SUBJ_CN}" ]]; then
  SUBJ_SAN+=",DNS:${SLUG}.${LEVEL}.${TLD}"
fi
SUBJ_SAN+="\n"
TMP_CONFIG="$(mktemp)"
cat ${CA_PATH}/intermediate/openssl.cnf > ${TMP_CONFIG}
printf "${SUBJ_SAN}" >> ${TMP_CONFIG}



declare -A paths
paths['key']="${CA_PATH}/intermediate/private/${SLUG}.key"
paths['insecure_key']="${CA_PATH}/intermediate/private/${SLUG}.insecure.key"
paths['req']="${CA_PATH}/intermediate/req/${SLUG}.csr"
paths['cert']="${CA_PATH}/intermediate/certs/${SLUG}.crt"
paths['pem']="${CA_PATH}/intermediate/certs/${SLUG}.pem"



declare -A human_explanation
human_explanation['key']="Certificate private key for ${SLUG}, passphrase is ${KEY_SECRET}"
human_explanation['insecure_key']="Certificate private key for ${SLUG}, not passphrase protected"
human_explanation['req']="Certificate request for ${SLUG}, as ${SUBJ_CN}. Might not be useful when we use self-signed"
human_explanation['cert']="Certificate for ${SLUG}, as ${SUBJ_CN}."
human_explanation['pem']="A PEM encoded certificate (i.e. key and crt in one file)"



# Add to this array what gotten created
declare -a created



GENRSA="\
openssl genrsa -aes256 \
               -passout env:KEY_SECRET \
               -out ${paths['key']} 2048"

if [[ ${SHOW_COMMANDS} == "1" ]]; then
  echo ${GENRSA}
fi

if [[ ! -f ${paths['key']} ]]; then
  ${GENRSA} && \
  created=(${created[@]} 'key')
fi



MAKE_INSECURE_KEY="openssl rsa -passin env:KEY_SECRET -in ${paths['key']} -out ${paths['insecure_key']}"
if [[ ${SHOW_COMMANDS} == "1" ]]; then
  echo ${MAKE_INSECURE_KEY}
fi

if [[ ! -f ${paths['insecure_key']} ]]; then
  ${MAKE_INSECURE_KEY} && \
  created=(${created[@]} 'insecure_key')
fi



CERTIFICATE_REQUEST="\
openssl req -new -sha256 -batch \
            -passin env:KEY_SECRET \
            -config ${CA_PATH}/intermediate/openssl.cnf \
            -subj ${SUBJ_LINE} \
            -key ${paths['key']} \
            -out ${paths['req']}"

if [[ ${SHOW_COMMANDS} == "1" ]]; then
  echo ${CERTIFICATE_REQUEST}
fi

if [[ ! -f ${paths['req']} ]]; then
  (${CERTIFICATE_REQUEST}) && \
  created=(${created[@]} 'req')
fi



REVIEW_REQUEST="openssl req -noout -text -in ${paths['req']}"

if [[ ${SHOW_COMMANDS} == "1" ]]; then
  echo ${REVIEW_REQUEST}
fi

if [[ ! -f ${paths['req']}.txt ]]; then
  ${REVIEW_REQUEST} > ${paths['req']}.txt
fi



if [[ ${CSR_ONLY} == "1" ]]; then
  exit 0
fi


SIGN_CERTIFICATE="\
openssl ca -config ${TMP_CONFIG} \
           -passin env:INTERMEDIATE1_CA_SECRET \
           -keyfile ${CA_PATH}/intermediate/private/intermediate.key \
           -extensions server_cert \
           -days 375 \
           -batch -utf8 -updatedb \
           -in ${paths['req']} \
           -out ${paths['cert']}"

if [[ ${SHOW_DETAILS} == "1" ]]; then
  echo ${SIGN_CERTIFICATE}
fi

if [[ ! -f ${paths['cert']} ]]; then
  $(${SIGN_CERTIFICATE}) && \
  created=(${created[@]} 'cert')
fi



REVIEW_CERT="openssl x509 -noout -text -in ${paths['cert']}"

if [[ ${SHOW_COMMANDS} == "1" ]]; then
  echo ${REVIEW_CERT}
fi

if [[ ! -f ${paths['cert']}.txt ]]; then
  ${REVIEW_CERT} > ${paths['cert']}.txt
fi



if [[ ! -f ${paths['pem']} ]]; then
  cat ${paths['insecure_key']} ${paths['cert']} > ${paths['pem']} && \
  created=(${created[@]} 'pem')
fi



if [[ ${#created[@]} -gt 0 ]]; then
  openssl verify -CAfile ${CA_PATH}/intermediate/certs/ca-chain.pem ${paths['pem']}
fi



if [[ ${SHOW_COMMANDS} == "1" ]]; then
  if [[ ${#created[@]} -gt 0 ]]; then
    echo "New files created:"
    for k in ${created[@]}; do
      echo "  - ${paths[${k}]}: ${human_explanation[${k}]}"
    done
  else
    echo 'No files created'
  fi
fi



rm ${TMP_CONFIG}
