'base':

  '*':
    - ppa
    - salt
    - sysdig
    - basesystem
    - cron

  'noc* and G@level:vagrant':
    - match: compound
    - level.vagrant.noc

  'noc*':
    - projects
    - salt.master
    - postfix.mx
    - openssl
    - openssl.certificates
    - stacks.mysql
    - stacks.nodejs
    - stacks.php
    - stacks.ruby

  ## For Web Developers who aren't working on server related changes
  ## that involves Salt Stack.
  ## We want a VM that has all we need to build web sites without touching
  ## server configs.
  'G@id:web-dev and G@level:vagrant':
    - match: compound
    - openssl
    - openssl.certificates
    - openssl.deploy
    - stacks.mysql.server
    - level.vagrant.db
    - level.vagrant.web-dev
    - salt.bash_aliases

  'web*':
    - stacks.apache2
    - stacks.apache2.vhosts
    - stacks.mysql
    - stacks.nodejs
    - stacks.php
    - stacks.ruby

  'web* and G@level:vagrant':
    - match: compound
    - level.vagrant.web

  '* and not G@level:vagrant':
    - match: compound
    - fail2ban

  ## Lets have a db server only when in Vagrant, on a Cloud Provider
  ## we are most likely going to use a hosted service.
  'db* and G@level:vagrant':
    - match: compound
    - stacks.mysql.server
    - level.vagrant.db

  'varnish* and G@level:vagrant':
    - match: compound
    - level.vagrant.varnish

  'level:vagrant':
    - match: grain
    - level.vagrant
