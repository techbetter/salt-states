#
# For all `setup_n.x` in https://github.com/nodesource/distributions/tree/master/deb
# Replace for `node_n.x` at the end of https://deb.nodesource.com/
#
# Supported as of 2016-07-28:
#   - 4.x
#   - 5.x
#   - 6.x
#
# Example: setup_6.x would be https://deb.nodesource.com/node_6.x
#
{%- set nodesource_version_slug = salt['pillar.get']('local:version_slugs:nodesource', 'node_4.x') %}
{%- set oscodename = salt['grains.get']('oscodename') %}

Setup Node.js dependencies:
  pkgrepo.managed:
    - humanname: nodesource
    - name: deb https://deb.nodesource.com/{{ nodesource_version_slug }} {{ oscodename }} main
    - file: /etc/apt/sources.list.d/nodesource-{{ oscodename }}.list
    - key_url: https://deb.nodesource.com/gpgkey/nodesource.gpg.key
  pkg.installed:
    - pkgs:
      - rlwrap
      - nodejs

Install global npm packages:
  cmd.run:
    - name: |
        npm install npm -g && \
        npm config set prefix "/srv/webapps/npm_packages" -g && \
        npm install -g bower grunt-cli gulp && \
        echo "changed=yes comment='NPM, Bower, Gulp and Grunt had been installed'"
    - unless: test -f /srv/webapps/npm_packages/bin/grunt
    - quiet: True

/etc/profile.d/C5_npm_packages.sh:
  file.managed:
    - group: users
    - onlyif: test -d /srv/webapps/npm_packages/bin
    - contents: |
        export PATH="/srv/webapps/npm_packages/bin:$PATH"
