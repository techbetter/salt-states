{%- if salt['grains.get']('level', 'production') != 'production' %}

{%-   for slug,obj in salt['pillar.get']('projects', {}).items() %}

{#      KEEP in sync with stacks.mysql.helpers.wordpress.anonymize #}
{%-     set pillar_key = 'mysql:projects:%s'|format(slug) %}
{%-     set database_credentials = salt['pillar.get'](pillar_key, none) %}

{%-     if database_credentials is not none %}

{%-       if database_credentials.get('database') %}
{%-         set database_name = database_credentials.get('database') %}
{%-       else %}
{%-         set database_name = slug %}
{%-       endif %}

Anonymize {{ database_name }} database:
  module.run:
    - name: mysql.query
    - database: {{ database_name }}
    - query: |
        UPDATE `wp_users` SET `user_pass` = '$P$BK/3Yafeg72Ziu2G72pMqtRPcZoBKj.'; -- admin is the password for ALL users

{%-     endif %}

{%-   endfor %}

{%- else %}

We should not run anonymize when we are in production:
  test.fail_without_changes:
    - name: 'We are currently in level:{{ salt['grains.get']('level', 'production') }}'

{%- endif %}
