include:
  - stacks.mysql

mysqld:
  pkg.installed:
    - name: mysql-server
    - require:
      - pkg: mysql
  service.running:
    - name: mysql
    - reload: True
    - enable: True

Ensure MySQL Server can listen on network:
  cmd.run:
    - name: "sed -i 's/^bind-address.*$/bind-address = 0.0.0.0/g' /etc/mysql/my.cnf"
    - unless: grep -qe '0\.0\.0\.0' /etc/mysql/my.cnf
    - watch:
      - pkg: mysqld

{#
 # This is an alternate way to include a state.
 # Issue with this method is that if in a state
 # run you included it, salt will complain
 # about duplicate entries. That's because
 # Jinja will render those at a different time
 # than with Salt's include statement.
 #
 # It's OK here because we have "stacks.mysql.databases"
 # state to be run manually too.
 #}
{% include "stacks/mysql/databases.sls" %}
