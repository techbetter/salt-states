{%- set php_version = salt['pillar.get']('local:version_slugs:ondrej_php', '5.6') %}

{%- set nodeName = salt['grains.get']('id') -%}
{%- set level = salt['grains.get']('level', 'local') -%}
{%- set tld = salt['pillar.get']('local:tld', 'localhost.local') %}
{%- set contact_email = salt['pillar.get']('local:contact_email', 'root@' ~ tld) -%}

include:
  - ppa

Apache Web Server:
  pkg.installed:
    - pkgs:
      - apache2
      - apache2-utils
      - libapache2-mod-php{{ php_version }}
  service.running:
    - name: apache2
    - reload: True
    - enable: True

/etc/apache2/conf-available/common.conf:
  file.managed:
    - source: salt://stacks/apache2/files/common.conf.jinja
    - template: jinja
    - mode: 644
    - context:
        level: {{ level }}
        tld: {{ tld }}
        nodeName: {{ nodeName }}
        contact_email: {{ contact_email }}
    - watch_in:
      - service: Apache Web Server

/etc/apache2/conf-available/mime_missing.conf:
  file.managed:
    - source: salt://stacks/apache2/files/mime_missing.conf
    - watch_in:
      - service: Apache Web Server

{%- for confName in [
   'mime_missing'
  ,'common'
] %}
Enable apache2 {{ confName }} conf:
  apache_conf.enabled:
    - name: {{ confName }}
    - unless: test -L /etc/apache2/conf-enabled/{{ confName }}.conf
    - require:
      - file: /etc/apache2/conf-available/{{ confName }}.conf
    - watch_in:
      - service: Apache Web Server
{%- endfor %}

{%- for modName in [
   'ssl'
  ,'env'
  ,'status'
  ,'rewrite'
  ,'headers'
  ,'setenvif'
  ,'mime_magic'
  ,'expires'
  ,'unique_id'
] %}
{#-
 # Don't run at every application.
 # Why would we disable that module manually anyway.
 #}
Enable apache2 {{ modName }} module:
  apache_module.enabled:
    - name: {{ modName }}
    - unless: test -L /etc/apache2/mods-enabled/{{ modName }}.load
    - watch_in:
      - service: Apache Web Server
{%- endfor %}

Enable apache2 php{{ php_version }} module:
  apache_module.enabled:
    - name: php{{ php_version }}
    - unless: test -L /etc/apache2/mods-enabled/php{{ php_version }}.load
    - watch_in:
      - service: Apache Web Server

Disable apache2 php5 module if we have php{{ php_version }}:
  apache_module.disabled:
    - name: php5
    - onlyif: |
        test -L /etc/apache2/mods-enabled/php5.load && \
        test -L /etc/apache2/mods-enabled/php{{ php_version }}.load
    - watch_in:
      - service: Apache Web Server

Remove php serializer in /etc/php/{{ php_version }}/mods-available/memcached.ini:
  cmd.run:
    - name: |
        sed -i "s/^memcached\.serializer.*$//g" /etc/php/{{ php_version }}/mods-available/memcached.ini
    - onlyif: grep -q -e "memcached\.serializer" /etc/php/{{ php_version }}/mods-available/memcached.ini
    - watch_in:
      - service: Apache Web Server

Remove default settings in /etc/apache2/conf-enabled/security.conf:
  apache_module.disabled:
    - name: security
    - onlyif: test -L /etc/apache2/conf-enabled/security.conf
    - watch_in:
      - service: Apache Web Server

/var/www/html/index.html:
  file.managed:
    - source: salt://stacks/apache2/files/index.html
