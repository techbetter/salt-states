include:
  - stacks.apache2

/etc/apache2/wordpress.conf:
  file.managed:
    - source: salt://stacks/apache2/files/wordpress.conf

{%- set level = salt['grains.get']('level', 'local') %}
{%- set tld = salt['pillar.get']('local:tld', 'localhost.local') %}
{%- set contact_email = salt['pillar.get']('local:contact_email', 'root@' ~ tld) -%}

{%- set apache_tls_certs_path = '/etc/apache2/tls' %}

{%- for slug,obj in salt['pillar.get']('projects', {}).items() %}

{%-   set server_name = obj.get('server_name', None) %}
{%-   set tls = obj.get('tls', None) %}
{%-   set server_aliases = obj.get('server_aliases', []) %}
{%-   set public_docroot = obj.get('public_docroot') %}
{%-   set apache = obj.get('apache', {}) %}
{%-   set wordpress = obj.get('wordpress', {}) %}

{%-   if public_docroot is none %}
{%-     set public_docroot_path = '/srv/www/' ~ slug ~ '/current' %}
{%-   else %}
{%-     set public_docroot_path = '/srv/www/' ~ slug ~ '/current/' ~ public_docroot %}
{%-   endif %}

Ensure project {{ slug }} apache2 projects.d file is in place:
  file.managed:
    - name: /etc/apache2/projects.d/{{ slug }}.conf
    - source: salt://stacks/apache2/files/project.conf.jinja
    - template: jinja
    - makedirs: True
    - context:
        apache: {{ apache }}
        wordpress: {{ wordpress }}
        slug: {{ slug }}
        level: {{ level }}
        tld: {{ tld }}
        server_aliases: {{ server_aliases }}
        public_docroot: {{ public_docroot_path }}
        contact_email: {{ contact_email }}
    ## Reload apache2 webserver if we changed VirtualHosts included configuration
    - watch_in:
      - service: Apache Web Server


{%-   if tls %}
Ensure project {{ slug }} apache2 SSL/TLS VirtualHost is in place:
  file.managed:
    - name: /etc/apache2/projects.d/{{ slug }}.tls.conf
    - source: salt://stacks/apache2/files/project.tls.conf.jinja
    - template: jinja
    - makedirs: True
    - context:
        apache: {{ apache }}
        wordpress: {{ wordpress }}
        slug: {{ slug }}
        apache_tls_certs_path: {{ apache_tls_certs_path }}
    ## Reload apache2 webserver if we changed VirtualHosts included configuration
    - watch_in:
      - service: Apache Web Server
{%-   endif %}

{%-   if server_name %}
{%-     if server_name == '.' %}
{%-       set server_name = '%s.%s.%s'|format(slug, level, tld) %}
{%-     endif %}

Ensure project {{ slug }} apache2 VirtualHost is in place:
  file.managed:
    - name: /etc/apache2/sites-available/{{ slug }}.conf
    - source: salt://stacks/apache2/files/site.conf.jinja
    - template: jinja
    - makedirs: True
    - context:
        apache: {{ apache }}
        slug: {{ slug }}
        level: {{ level }}
        tld: {{ tld }}
        server_name: {{ server_name }}
        tls: {{ tls }}
    ## Reload apache2 webserver if we changed VirtualHosts included configuration
    - watch_in:
      - service: Apache Web Server
{%-   endif %}

{%- endfor %}
