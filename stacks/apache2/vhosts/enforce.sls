include:
  - stacks.apache2
  - stacks.apache2.vhosts

{%  for slug,obj in salt['pillar.get']('projects', {}).items() %}

{%-   set server_name = obj.get('server_name', None) %}
{%-   if server_name %}
Enable {{ slug }} VirtualHost:
  apache_site.enabled:
    - name: {{ slug }}
    - unless: test -L /etc/apache2/sites-enabled/{{ slug }}.conf
    - order: last
    - watch_in:
      - service: Apache Web Server
{%-   endif %}

{%-   set admin_virtualhost = obj.get('admin_virtualhost') %}
{%-   set tls = obj.get('tls', None) %}
{%-   if admin_virtualhost %}
{%-     if tls %}
Enable {{ slug }}.admin VirtualHost:
  apache_site.enabled:
    - name: {{ slug }}.admin
    - unless: test -L /etc/apache2/sites-available/{{ slug }}.admin.conf
    - order: last
    - watch_in:
      - service: Apache Web Server
{%-     else %}
Enable {{ slug }}.admin VirtualHost has not been enforced:
  test.fail_without_changes:
    - name: 'We did not enable {{ slug }}.admin because we did not enable TLS. See pillar projects:{{ slug }}:tls if it is set.'
{%-     endif %}
{%-   endif %}

{%- endfor %}

{#
 # Due to generation order, the following command is ran **after**
 # Enable {{ slug }} above.
 # To enforce this fact, I'm putting it below even though it would be
 # ideal to not have to do this.
 #}
rm /etc/apache2/sites-enabled/*.conf:
  cmd.run:
    - onlyif: test -d /etc/apache2/sites-enabled/
    - order: 1
