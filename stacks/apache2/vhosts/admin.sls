{%- set level = salt['grains.get']('level', 'local') -%}
{%- set tld = salt['pillar.get']('local:tld', 'localhost.local') %}

{%- set mine_get_noc_internal_ip_addrs = salt['mine.get']('noc*', 'internal_ip_addrs') %}
{%- set noc_internal_ipv4 = mine_get_noc_internal_ip_addrs.noc[0] %}

{%- for slug,obj in salt['pillar.get']('projects', {}).items() %}
{%-   if obj.admin_virtualhost is defined %}
{%-     set apache = obj.get('apache', {}) %}

Ensure project Admin apache2 VirtualHost for {{ slug }} is in place:
  file.managed:
    - name: /etc/apache2/sites-available/{{ slug }}.admin.conf
    - source: salt://stacks/apache2/files/site.admin.conf.jinja
    - template: jinja
    - context:
        apache: {{ apache }}
        slug: {{ slug }}
        level: {{ level }}
        tld: {{ tld }}
        obj: {{ obj }}
        noc_internal_ipv4: {{ noc_internal_ipv4 }}

{%-   endif %}
{%- endfor %}
