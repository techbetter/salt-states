## Remove this include line if we put stacks.apache2.fastly as part of highstate.
include:
  - stacks.apache2

{% set fastly_ip_list =  salt['http.query']('https://api.fastly.com/public-ip-list') %}

/etc/apache2/fastly.conf:
  file.managed:
    - template: jinja
    - source: salt://stacks/apache2/files/fastly.conf.jinja
    - context:
        test: {{ fastly_ip_list }}
    - watch_in:
      - service: Apache Web Server
