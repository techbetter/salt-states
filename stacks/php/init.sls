{%- set php_version = salt['pillar.get']('local:version_slugs:ondrej_php', '5.6') %}

include:
  - ppa

Setup PHP {{ php_version }} from Ondrej Sury PHP PPA:
  pkg.installed:
    - install_recommends: False
    - refresh: True
    - require:
      - pkgrepo: Use ondrej/php PPA
    - pkgs:
      - php{{ php_version }}
      - php{{ php_version }}-gd
      - php{{ php_version }}-cli
      - php{{ php_version }}-xml
      - php{{ php_version }}-gmp
      - php{{ php_version }}-curl
      - php{{ php_version }}-mysql
      - php{{ php_version }}-mcrypt
      - php{{ php_version }}-opcache
      - php{{ php_version }}-mbstring
      - php-memcached
      - php-igbinary
      - php-apcu

install-composer:
  pkg.installed:
    - name: curl
  cmd.run:
    - name: |
         curl -sS https://getcomposer.org/installer -o /root/composer-installer && \
         php /root/composer-installer --filename=composer --install-dir=/usr/bin && \
         echo "changed=yes comment='Composer has been installed'"
    - unless: test -f /usr/bin/composer
    - env:
      - COMPOSER_HOME: /srv/webapps

/etc/php/{{ php_version }}/mods-available/serialization.ini:
  file.managed:
    - contents: |
        ; Managed by Salt Stack.
        ; Set the default serializer for new memcached objects.
        ; valid values are: php, igbinary, json, json_array, msgpack
        ;
        ; json - standard php JSON encoding. This serializer
        ;        is fast and compact but only works on UTF-8
        ;        encoded data and does not fully implement
        ;        serializing. See the JSON extension.
        ; json_array - as json, but decodes into arrays
        ; php - the standard php serializer
        ; igbinary - a binary serializer
        ; msgpack - a cross-language binary serializer
        ;
        ; The default is igbinary if available, then msgpack if available, then php otherwise.
        memcached.serializer=igbinary
        apc.serializer=igbinary
        session.serialize_handler=igbinary
  cmd.run:
    - name: phpenmod serialization
    - creates: /etc/php/{{ php_version }}/apache2/conf.d/20-serialization.ini


{#
 # Review PHP deployment #TODO
 #
 # https://phpbestpractices.org/#utf-8
 # http://www.phptherightway.com/#error_reporting_title
 #}
