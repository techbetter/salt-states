{%- set php_version = salt['pillar.get']('local:version_slugs:ondrej_php', '5.6') %}

include:
  - ppa
  - stacks.php

Install packaging dependencies:
  pkg.installed:
    - refresh: True
    - require:
      - pkgrepo: Use ondrej/php PPA
    - pkgs:
      - php{{ php_version }}-xml
      - build-essential
      - libmagic-dev
      - php-all-dev
      - debhelper
      - php-pear
      - dh-php
  cmd.run:
    - stateful: True
    - creates: /usr/share/php/.channels/pear.php.net.reg
    - name: |
        pear config-set php_ini /etc/php/{{ php_version }}/cli/php.ini && \
        pear config-set php_ini /etc/php/{{ php_version }}/apache2/php.ini && \
        echo "changed=yes comment='PEAR has been installed'"

{#
 # Incomplete, see:
 # - https://www.dotdeb.org/2008/09/25/how-to-package-php-extensions-by-yourself/
 # - https://github.com/oerdnj/deb.sury.org/wiki/PECL-considerations
 # - https://github.com/jordansissel/fpm/wiki
 # - https://github.com/jordansissel/fpm/wiki/PackageSimpleFiles
 #}
