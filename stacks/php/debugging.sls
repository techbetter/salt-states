{%- set xhprof_version = '0.9.4' %}
{%- set php_version = salt['pillar.get']('local:version_slugs:ondrej_php', '5.6') %}

include:
  - ppa
  - stacks.php

PHP Debug tools:
  pkg.installed:
    - refresh: True
    - require:
      - pkgrepo: Use ondrej/php PPA
    - pkgs:
      - php{{ php_version }}-phpdbg
      - php{{ php_version }}-dev
      - php{{ php_version }}-xml
  cmd.run:
    - name: |
        pecl channel-update pecl.php.net && \
        pecl install channel://pecl.php.net/xhprof-{{ xhprof_version }} && \
        echo "changed=yes comment='XHProf has been installed' version={{ xhprof_version }}"
    - stateful: True
    - template: jinja
    - creates: /usr/share/php/.registry/.channel.pecl.php.net/xhprof.reg

/etc/php/{{ php_version }}/mods-available/xhprof.ini:
  file.managed:
    - contents: |
        extension=xhprof.so
  cmd.run:
    - name: phpenmod xhprof
    - creates: /etc/php/{{ php_version }}/apache2/conf.d/20-xhprof.ini

{#
# See about other tracing and debugging tools
#
#  - https://github.com/arnaud-lb/php-memory-profiler/blob/master/README.md
#  - https://github.com/patrickallaert/php-apm/blob/master/README.md
#  - https://www.sitepoint.com/the-need-for-speed-profiling-with-xhprof-and-xhgui/
#  - https://www.phase2technology.com/blog/profiling-in-production-whats-slowing-you-down/
#  - http://stackoverflow.com/questions/35476889/memory-usage-of-php-process
#  - http://raveren.github.io/kint/
#  - http://php.net/manual/en/mbstring.installation.php
#  - https://github.com/tony2001/pinba_engine/wiki/Basics#How_it_works
#  - https://tournasdimitrios1.wordpress.com/2010/11/27/bandwidth-monitoring-with-vnstat-and-php-gd/
#
# Go to:
#  - http://127.0.0.1:8080/wp-content/plugins/wp-xhprof-profiler/facebook-xhprof/xhprof_html/
#  - http://127.0.0.1:8080/webgrind/
#  - http://127.0.0.1:8080/wp-content/plugins/wp-xhprof-profiler/facebook-xhprof/xhprof_html/docs/index.html
#
# Try?: kcachegrind might be heavy or https://josephscott.org/archives/2013/07/qcachegrind-kcachegrind-on-mac-os-x/ on OS X
#
# https://wordpress.org/plugins/wp-xhprof-profiler/
# https://www.sitepoint.com/the-need-for-speed-profiling-with-xhprof-and-xhgui/
# https://blog.engineyard.com/2014/profiling-with-xhprof-xhgui-part-1
# https://github.com/corretge/xdebug-trace-gui
# https://github.com/tungnguyenson/xdebug-trace-explorer
# https://github.com/jokkedk/webgrind
#
# Apache VirtualHost:
#    php_value 'xdebug.auto_trace' 'On'
#    php_value 'xdebug.profiler_enable' 'On'
#    php_value 'xdebug.profiler_enable_trigger' 'On'
#    php_value 'xdebug.profiler_output_dir' '/tmp'
#
# see also ?_profile=1
#
#php_admin_value auto_prepend_file "/vagrant/xhgui/external/header.php"
#Alias /xhgui /vagrant/xhgui/xhprof_html
#<Directory "/vagrant/xhgui/xhprof_html">
#  AllowOverride All
#  Options Indexes FollowSymLinks
#  Require all granted
#</Directory>
#}
