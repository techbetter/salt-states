include:
  - ppa
  - stacks.php

## See to use the following instead https://github.com/vrana/adminer/archive/v4.2.5.zip
adminer:
  pkg.installed:
    - version: 4.2.5
    - refresh: True
    - skip_verify: True
    - allow_updates: False
    - skip_suggestions: True
    - hold: True
    - require:
      - file: /etc/apt/sources.list.d/fury_betastream.list
  file.symlink:
    - name: /etc/apache2/mods-enabled/adminer.conf
    - target: /etc/apache2/mods-available/adminer.conf
