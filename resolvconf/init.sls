{#
 # Make a machine use the NOC as a DNS resolver
 #
 # See README.md
 #}

{%- set mine_internal_ips_noc = salt['mine.get']('role:noc', 'internal_ip_addrs', expr_form='grain') %}
{%- set list_length = mine_internal_ips_noc|count() %}

{#
 #    Assuming we have one NOC, with ONE private IPv4 address, we will update
 #    all other minions to use that NOC as a DNS resolver.
 #}
{%- if list_length == 1 %}


{#
 #    This section is about backing up initial DNS nameservers we we can prepend
 #    our own
 #}
{%-   if salt['grains.get']('original:dns:ip4_nameservers') == "" %}


{%-     set original_ip4_nameservers = salt['grains.get']('dns:ip4_nameservers') %}

We did not backup original original:dns:ip4_nameservers yet:
  test.succeed_without_changes:
    - name: 'They are: {{ original_ip4_nameservers|json }}'

original:dns:ip4_nameservers:
  grains.present:
    - value: {{ original_ip4_nameservers }}


{%-   else %}
{%-     set original_ip4_nameservers = salt['grains.get']('original:dns:ip4_nameservers') %}
{%-   endif %}


{#
 #    Convert as an array so we do not need to guess the
 #    NOC name from the structured data
 #
 #    For example:
 #
 #        {"noc": ["172.28.128.6"]}
 #
 #    Applying .items() to it would give
 #
 #        [["noc", ["172.28.128.6"]]]
 #
 #    Where we can now extract the [0][1][0]th element; only the IP address.
 #
 #}
{%-   set noc_internal_ip4 = mine_internal_ips_noc.items()[0][1][0] %}

Expecting exactly one NOC, with exactly one private IP, we got {{ noc_internal_ip4 }}:
  test.succeed_without_changes:
    - name: 'Extracted from {{ mine_internal_ips_noc|json }}'

master_internal_ip4:
  grains.present:
    - value: {{ noc_internal_ip4 }}

resolvconf:
  pkg:
    - installed

/etc/resolvconf/resolv.conf.d/head:
  file.managed:
    - mode: '0644'
    - source: salt://resolvconf/files/resolv.conf.jinja
    - template: jinja
    - context:
        nameservers: {{ salt['pillar.get']('resolvconf:nameservers', [noc_internal_ip4]) }}
        searchpaths: {{ [] }}
        options: {{ [] }}

/etc/resolvconf/resolv.conf.d/base:
  file.managed:
    - mode: '0644'
    - source: salt://resolvconf/files/resolv.conf.jinja
    - template: jinja
    - context:
        nameservers: {{ [] }}
        searchpaths: {{ salt['pillar.get']('resolvconf:searchpaths', [salt['grains.get']('domain'), 'local']) }}
        options: {{ salt['pillar.get']('resolvconf:options', []) }}

resolvconf -u:
  cmd.run:
    - order: last
    - watch:
      - file: /etc/resolvconf/resolv.conf.d/head
      - file: /etc/resolvconf/resolv.conf.d/base


{%- else %}


We cannot setup resolvconf, we do not have a NOC to use as DNS server. (NB If it is first time, do not worry):
  test.fail_without_changes:
    - name: Unexpected list of IP length {{ list_length }} !== 1, from: {{ mine_internal_ips_noc|json }}


{%- endif %}
