# Make a machine use the NOC as a DNS resolver

This state is about editing current machine to regenerate /etc/resolv.conf
where we make the NOC to be the first name resolver, right before the one
already provided by the Cloud Platform.

Run this state manually on the NOC once you have it fully installed and with gdnsd,
(see Dependencies) then run the same state on other machines from the cluster.


## Dependencies:

- gdnsd.master


## Checks before running

1. Figure out which ethernet adapter is the one you use for the internal network.
   Here we will be using `eth1`

    ifconfig
    eth1      Link encap:Ethernet  HWaddr 08:00:27:32:35:29
              inet addr:172.28.128.5  Bcast:172.28.128.255  Mask:255.255.255.0
              inet6 addr: fe80::a00:27ff:fe32:3529/64 Scope:Link
              UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
              RX packets:2654 errors:0 dropped:0 overruns:0 frame:0
              TX packets:2040 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:1000
              RX bytes:606713 (606.7 KB)  TX bytes:893698 (893.6 KB)


1. IP in `internal_ip_addrs` mine for noc should match
    what the declares.

     salt-run mine.get '*' internal_ip_addrs

       noc:
           ----------
           noc:
               - 172.28.128.5
           web:
               - 172.28.128.6
       web:
           ----------
           noc:
               - 172.28.128.5
           web:
               - 172.28.128.6

     dig A noc.vagrant.alias.services +noall +answer @localhost

       noc.vagrant.alias.services. 3600 IN	A	172.28.128.5


1. Ensure the domain grain is matching the combination of the level and tld

      salt-call pillar.get local:tld
      local:
        azure.alias.services
      salt-call grains.get level
      local:
        dev
      salt-call grains.get domain
      local:
        dev.azure.alias.services


1. If the above is not similar, you can rewrite the automatically detected grain:

      salt-call grains.set domain dev.azure.alias.services
      local:
          ----------
          changes:
              ----------
              domain:
                dev.azure.alias.services
          comment:
          result:
              True


## Run

If the `domain` grain did not match and you had to rewrite it on the NOC, we'll have to
make minions aware of it;

    salt \* grains.set domain dev.azure.alias.services


On a NOC

    salt-call -l info state.sls gdnsd.master
    salt-call -l info state.sls resolvconf
    salt \* state.sls resolvconf
