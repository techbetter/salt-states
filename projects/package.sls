{%- if 'noc' in salt['grains.get']('id') %}

{%-   for slug,obj in salt['pillar.get']('projects', {}).items() %}
{%-     set project_clone_path = '/srv/webapps/projects/' ~ slug  %}
Package {{ project_clone_path }}:
  file.directory:
    - name: /srv/salt/files/projects
  cmd.run:
    - cwd: {{ project_clone_path }}
    - template: jinja
    - unless: test -f /srv/salt/files/projects/{{ slug }}-latest.tar.bz2
    ## Exclude node_modules? Not really, in some contexts we will want to have them in. See Jira issue #2176
    - name: tar cfj /srv/salt/files/projects/{{ slug }}-latest.tar.bz2 . --exclude-vcs
  event.send:
    - name: projects/deploy/{{ slug }}/packaged
    - onchanges:
      - cmd: Package {{ project_clone_path }}
    - data:
        packaged: "{{ slug }}"
{%-   endfor %}

{%- else %}{#- End If 'noc' in salt['grains.get']('id') #}

Ensure we run NOC specific sls where it should:
  test.fail_without_changes:
    - name: State script not meant to be run on other node type than the NOC

{%- endif %}{#- End Else of If 'noc' in salt['grains.get']('id') #}
