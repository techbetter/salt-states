{%- if salt['grains.get']('id') in ['noc', 'web-dev'] %}

{%- if 'web-dev' in salt['grains.get']('id') %}
{%-   set project_repo_dir = '/srv/www/%s/current' -%}
{%- else %}
{%-   set project_repo_dir = '/srv/webapps/projects/%s' -%}
{%- endif %}

{%-   from "basesystem/macros/git.sls" import git_latest -%}

{%-   set settings = {
          "mirror": True
} %}

{%-   for slug,obj in salt['pillar.get']('projects', {}).items() %}

{%-     do obj.update(settings) %}

{%-     if obj.origin is defined %}
{%-       set repo_dir = project_repo_dir|format(slug) %}
{%-       if obj.identity is not defined %}
{%-         set add_identity = {
                  "identity": "/srv/webapps/.ssh/id_rsa"
            } %}
{%-         do obj.update(add_identity) %}
{%-       endif %}
{{        git_latest(repo_dir, obj.origin, obj) }}
{%-     endif %}

{%-     set dependencies = obj.get('dependencies', none) %}
{%-     if dependencies is not none %}
{%-       set apt_dependencies = dependencies.get('apt', none) %}
{%-       if apt_dependencies is not none %}
Ensure APT dependencies for {{ slug }} are installed:
  pkg.installed:
    - pkgs: {{ apt_dependencies|json }}
{%-       endif %}
{%-     endif %}

{%-   endfor %}

{%- else %}{#- End If 'noc' in salt['grains.get']('id') #}

Ensure we run NOC specific sls where it should:
  test.fail_without_changes:
    - name: State script not meant to be run on other node type than the NOC

{%- endif %}{#- End Else of If 'noc' in salt['grains.get']('id') #}
