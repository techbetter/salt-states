{%- if 'web' in salt['grains.get']('id') %}

{%-   set deployed_dir = '/srv/www' %}
{%-   set level = salt['grains.get']('level', 'local') -%}
{%-   set tld = salt['pillar.get']('local:tld', 'localhost.local') -%}

{%-   set use_mysql_sa_user_as_salt = salt['pillar.get']('mysql:service_account:username', 'root') %}
{%-   set database_endpoint = salt['pillar.get']('local:endpoints:mysql', 'localhost') -%}


{%-   for slug,obj in salt['pillar.get']('projects', {}).items() %}

{%-     set webapp_current_path = deployed_dir ~ '/' ~ slug ~ '/current' %}
{%-     set webapp_tmp_path = '/tmp/webapps/' ~ slug %}

{%-     set pillar_key = 'mysql:projects:%s'|format(slug) %}

{#
 #      Do not require web-dev VM to tell database credentials.
 #}
{%-     if 'web-dev' in salt['grains.get']('id') %}
{%-       set database_credentials = {
            "database": slug,
            "username": "root",
            "password": "",
            "hostname": "127.0.0.1"
          } %}
{%-     else %}
{#        KEEP in sync with stacks.mysql.helpers.wordpress.anonymize #}
{%-       set database_credentials = salt['pillar.get'](pillar_key, none) %}
{%-       if database_credentials %}
{%-         if database_credentials.get('endpoint') %}
{%-           set database_endpoint = database_credentials.get('endpoint') %}
{%-         endif %}
{%-       endif %}
{%-     endif %}

Is database_credentials defined for "projects:{{ slug }} at {{ pillar_key }}":
  test.succeed_without_changes:
    - name: 'Value was {{ database_credentials|json }}'

Is database_endpoint superseeded at {{ pillar_key }}":
  test.succeed_without_changes:
    - name: 'Value was {{ database_endpoint|json }}'

{%-     set proto = 'http' %}

{%-     set wordpress_specific = obj.get('wordpress') %}

{%-     set config_filename_dest = None %}
{%-     set renoirb_patch_dest = None %}
{#      Line below may look odd, but I could not do wordpress_context_root with
        None as default value, we gotta find why we have to send empty string later #}
{%-     set wordpress_context_root = ' ' %}

{%-     if wordpress_specific %}
{%-       set proto = wordpress_specific.get('proto', 'http') %}

{%-       if wordpress_specific.get('config_filename') %}
{%-         set config_filename_dest = wordpress_specific.get('config_filename') %}
{%-       endif %}
{%-       if wordpress_specific.get('renoirb_patch') %}
{%-         set renoirb_patch_dest = wordpress_specific.get('renoirb_patch') %}
{%-       endif %}
{%-       if wordpress_specific.get('context_root') %}
{%-         set wordpress_context_root = '/%s'|format(wordpress_specific.get('context_root')) %}
{%-       endif %}
{%-     endif %}

Has there WordPress specific settings for "projects:{{ slug }}":
  test.succeed_without_changes:
    - name: 'Value was {{ wordpress_specific|json }}'

{%-     if database_credentials is not none %}

{%-       if database_credentials.get('database') %}
{%-         set database_name = database_credentials.get('database') %}
{%-       else %}
{%-         set database_name = slug %}
{%-       endif %}

{%-       set add_database_name = {
            "database": database_name
          } %}
{%-       do database_credentials.update(add_database_name) %}

{%-       if config_filename_dest is not none %}
{%-         set salts = {
              'AUTH_KEY':'',
              'SECURE_AUTH_KEY':'',
              'LOGGED_IN_KEY':'',
              'NONCE_KEY':'',
              'AUTH_SALT':'',
              'SECURE_AUTH_SALT':'',
              'LOGGED_IN_SALT':'',
              'NONCE_SALT':''} %}

{#
 # Put first 64 first characters of randomly generated sha512 hashes
 # http://codex.wordpress.org/Editing_wp-config.php#Security_Keys
 #}
{%-         for salt_item_key_name in salts %}
{%-           set digest = salt['hashutil.digest']( slug ~ '-' ~ use_mysql_sa_user_as_salt ~ '-' ~ salt_item_key_name | lower, 'sha512' ) %}
{%-           do salts.update({salt_item_key_name: digest[:64]}) %}
{%-         endfor %}

{{ webapp_current_path }}/{{ config_filename_dest }}:
  file.managed:
    - source: salt://projects/type/wordpress/files/env-local.php.jinja
    - template: jinja
    - makedirs: True
    - context:
        slug: {{ slug }}
        level: {{ level }}
        tld: {{ tld }}
        proto: {{ proto }}
        database_credentials: {{ database_credentials }}
        database_endpoint: {{ database_endpoint }}
        webapp_tmp_path: {{ webapp_tmp_path }}
        wordpress_context_root: {{ wordpress_context_root }}
        salts: {{ salts }}
{%-       endif %}{#- End if config_filename_dest is not none #}
{%-     endif %}{#- End if database_credentials is not none #}

{%-     if renoirb_patch_dest is not none %}
{{ webapp_current_path }}/{{ renoirb_patch_dest }}:
  file.managed:
    - source: salt://projects/type/wordpress/files/renoirb.php
    - makedirs: True
{%-     endif %}

{%-   endfor %}{#- End salt['pillar.get']('projects', {}).items() #}

{%- else %}{#- End if 'web' in salt['grains.get']('id') #}

Ensure we run web specific sls where it should:
  test.fail_without_changes:
    - name: State script not meant to be run on other node type than Web Servers

{%- endif %}{#- End Else of If 'web' in salt['grains.get']('id') #}
