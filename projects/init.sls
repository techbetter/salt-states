{%- if 'noc' in salt['grains.get']('id') %}

Add projects list as a grain to Salt Master:
  grains.list_present:
    - name: projects
    - value:
{%    for project_identifier,project_data in salt['pillar.get']('projects', {}).iteritems() %}
      - {{ project_identifier }}
{%    endfor %}

{%- endif %}
