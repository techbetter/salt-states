<?php



/** KEEP IN SYNC WITH https://bitbucket.org/snippets/betastream/G6RM9 **/




/**
 * Must-Use plugins... functions to disable non needed WordPress features
 *
 * Those are things I generally disable to make the HTML be
 * cleaner and to remove things I don't need from WordPress
 *
 * Files I looked at:
 * - wp-content/plugins/yet-another-related-posts-plugin/classes/YARPP_Widget
 * - wp-content/plugins/yet-another-related-posts-plugin/yarpp.php
 * - wp-includes/canonical.php
 * - wp-content/plugins/wordpress-seo/frontend/class-json-ld.php
 * - wp-includes/link-template.php
 * - wp-includes/formatting.php
 * - wp-includes/theme.php
 **/



/**
 * Truncate protocol and domain name off an URL.
 *
 * That way we can support multiple domain names without
 * needing to vary cache based on domain name and protocol.
 **/
 function truncate_protocol_domain( $provided_url ) {
    /**
     * Match any of:
     *  - http://example.org/foo
     *  - https://example.org/foo
     *  - //example.org/foo
     *  - http://example.org:88008/bar
     *  - https://example.org:88008/bar
     *  - //example.org:88008/bar
     **/
     $pattern = '/^(?:https?:)?\/\/[\w\.\-_0-9]+\/(.*)/';
     preg_match($pattern, $provided_url, $matches);
     $out = (isset($matches[1])) ? '/'.$matches[1] : null;
     if (is_string($out) === false) {
       if (substr($provided_url, 0, 12) === '/wp-content/') {
          $out = $provided_url;
          return $out;
       }
       if (substr($provided_url, 0, 1) === '/' && substr($provided_url, 0, 2) !== '//') {
          $out = $provided_url;
          return $out;
       }
    }
    /**
     * Grab
     *  - /something/wp-content
     *  - /something/wp-includes
     *  - /something/wp-content/themes/foo/js/something.js
     *  - /wp-includes
     *  - /wp-content/themes/foo/js/something.js
     *
     * That will be useful once we have WordPress not directly exposed on the DocumentRoot
     *
     * $pattern = '/^\/([a-z]+\/)?((?:wp-content|wp-includes).*)/';
     **/
    #error_log('truncate_protocol_domain did not catch for: "'.$provided_url.'"');
    return $out;
 }

 add_filter( 'stylesheet_directory_uri', 'truncate_protocol_domain' );
 add_filter( 'script_loader_src', 'truncate_protocol_domain' );
 add_filter( 'theme_root_uri',  'truncate_protocol_domain' );
 add_filter( 'post_link',  'truncate_protocol_domain' );
 add_filter( 'plugins_url',  'truncate_protocol_domain' );

/**
 * Filter at home_url hook, make the URL be without protocol nor hostname.
 *
 * See wp-includes/link-template.php at get_home_url
 *
 * @param string      $url         The complete home URL including scheme and path.
 * @param string      $path        Path relative to the home URL. Blank string if no path is specified.
 * @param string|null $orig_scheme Scheme to give the home URL context. Accepts 'http', 'https',
 *                                 'relative', 'rest', or null.
 * @param int|null    $blog_id     Blog ID, or null for the current blog.
 */
 function truncate_protocol_domain_home($url, $path, $orig_scheme=null, $blog_id=null){

    /**
     * We only want to interact when this filter is called
     * and $orig_scheme is NOT set to "rest".
     **/
    if($orig_scheme !== 'rest') {
      if(empty($path)) {

        /**
         * Sometimes we get here no path, and an URL without trailing slash.
         * Let's remove host and protocol here instead of at truncate_protocol_domain
         **/
        $url = '/';
        //error_log('home_url filter had empty path, forcing to relative: '.$url);

        return $url;

      } else {

        /**
         * Had issues when $path either did NOT start with /, or did have two //
         * If you see errors like:
         *
         *     truncate_protocol_domain did not catch for: "http://main-sitemap.xsl/", referer: http://energyfactor.vagrant.alias.services/
         *
         * You can use this below to see what's happening.
         **/ /*
        preg_match('/main-sitemap\.xsl/', $url, $matches);
        if (isset($matches[0])) {
          error_log('Tyring to fix truncate_protocol_domain, had a match, url: "'.$url.'", path:"'.$path.'"');
        } */

        /**
         * If a plugin uses home_url( 'main-sitemap.xsl' )  without a forward slash (/)
         * like WordPress SEO plugin does at inc/class-sitemaps.php
         **/
        if(substr($path, 0, 1) !== '/') {
            //error_log('home_url filter, had:'.$url.', path:'.$path);
            $url = '/'.$path;

            return $url;
        }

        /**
         * If by some reason $path mistakenly to start by //, let's fix it.
         **/
        if(substr($path, 0, 2) === '//') {
            return substr($path,1);
        }

        /**
         * We do not want protocol and hostname, since path is already there
         * and already has the path, let's give it right away
         **/
        //error_log('home_url filter, url: "'.$url.'", path: "'.$path.'"');
        return $path;
      }
    }

    return $url;
 }



/**
 * See wp-includes/formatting.php at esc_url, at the end, the filter call
 *
 * @param string $good_protocol_url The cleaned URL to be returned.
 * @param string $original_url      The URL prior to cleaning.
 * @param string $_context          If 'display', replace ampersands and single quotes only.
 **/
function truncate_protocol_domain_clean($good_protocol_url, $original_url, $_context){
  //error_log('clean_url hook: {1: "'.$good_protocol_url.'", 2:"'.$original_url.'", 3:"'.$context.'"}');
  //error_log('clean_url hook: "'.$good_protocol_url.'"');
  /**
   * Must have at least 3 slashes, and possibly a path
   * 1. https://energyfactor.vagrant.alias.services/wp-json/"
   * 2. https://energyfactor.vagrant.alias.services/"
   * 3. https://energyfactor.cdnist.com/wp-content/uploads/2016/05/EnergyFactor_logo_400_RGB_Square1.png"
   * 4. //energyfactor.vagrant.alias.services/wp-content/plugins/yet-another-related-posts-plugin/style/widget.css?ver=4.4.2"
   * 5. /wp-includes/js/wp-embed.js?ver=4.4.2"
   */
   // Catch case #5
   if ( substr($good_protocol_url, 0, 1) === '/' && substr($good_protocol_url, 0, 2) !== '//') {
     //error_log('clean_url catch already relative: '.$good_protocol_url);
     return $good_protocol_url;
   }
   // Catch case #4
   if ( substr($good_protocol_url, 0, 2) === '//' ) {
     //error_log('clean_url catch protocol-relative: '.$good_protocol_url);
     return truncate_protocol_domain($good_protocol_url);
   }
   // Catch 1-3
   if ( substr_count($good_protocol_url,'/') < 3 && substr($good_protocol_url, -1) !== '/' ) {
     return truncate_protocol_domain($good_protocol_url);
   }
}



add_action( 'wp', function() {

  add_filter( 'home_url', 'truncate_protocol_domain_home', 10, 3 );
  add_filter( 'clean_url', 'truncate_protocol_domain_clean', 10, 3);

  /**
   * Remove unneeded wlmanifest and rsd links in head
   *
   * They're useful only if we use mobile WordPress editor apps
   **/
  remove_action('wp_head', 'wlwmanifest_link');
  remove_action('wp_head', 'rsd_link');

  /**
   * Remove the admin bar from frontend when logged-in, we do not need it
   **/
  show_admin_bar(false);

} );
