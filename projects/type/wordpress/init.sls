/usr/bin/wordpress-cron.sh:
  file.managed:
    - source: salt://projects/type/wordpress/files/cron.sh
    - user: www-data
    - group: www-data
    - mode: 755
