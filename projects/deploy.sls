{%- if 'web' in salt['grains.get']('id') %}

{%-   set deployed_dir = '/srv/www' %}
{%-   set level = salt['grains.get']('level', 'local') -%}
{%-   set tld = salt['pillar.get']('local:tld', 'localhost.local') -%}

include:
  - projects.type.wordpress
  - stacks.apache2.vhosts
  - openssl.deploy

{%-   for slug,obj in salt['pillar.get']('projects', {}).items() %}

{%-     set webapp_current_path = deployed_dir ~ '/' ~ slug ~ '/current' %}
{%-     set webapp_tmp_path = '/tmp/webapps/' ~ slug %}

{%-     set server_name = obj.get('server_name', None) %}
{%-     set tls = obj.get('tls', None) %}
{%-     set public_docroot = obj.get('public_docroot', None) %}
{%-     set wordpress = obj.get('wordpress', None) %}

{%-     if public_docroot is none %}
{%-       set public_docroot_slash = '' %}
{%-     else %}
{%-       set public_docroot_slash = public_docroot ~ '/' %}
{%-     endif %}

Deploy {{ webapp_current_path }} package:
  archive.extracted:
    - name: {{ webapp_current_path }}
    - source: salt://files/projects/{{ slug }}-latest.tar.bz2
    - archive_format: tar
    - tar_options: j
  event.send:
    - name: projects/deploy/{{ slug }}/extracted
    - onchanges:
      - archive: Deploy {{ webapp_current_path }} package
    - data:
        extracted: "{{ slug }}"

{%-     if server_name %}
Enable {{ server_name }} Apache VirtualHost for {{ slug }}:
  apache_site.enabled:
    - name: {{ slug }}
{%-     endif %}

{%-     if wordpress  %}

{{ webapp_current_path }}/{{ public_docroot_slash }}.htaccess:
  file.managed:
    - source: salt://projects/type/wordpress/files/htaccess.txt

{%-     set webapp_current_path_wordpress = '%s/%s'|format(webapp_current_path, public_docroot_slash) %}
{%-     set context_root = wordpress.get('context_root', None) %}
{%-     if context_root %}
{%-       set webapp_current_path_wordpress = webapp_current_path_wordpress ~ context_root ~ '/' %}
{%-     endif %}

Setting up CRON job for {{ webapp_current_path_wordpress }}:
  cron.present:
    - identifier: wp-cron-{{ slug }}
    - name: 'JOBNAME=wp-cron-{{ slug }} WP_ROOT={{ webapp_current_path_wordpress }} cronhelper.sh /usr/bin/wordpress-cron.sh'
    - user: www-data
    - minute: '*/2'
    - hour: '*'
{%-     endif %}

{%-     set dependencies = obj.get('dependencies', none) %}
{%-     if dependencies is not none %}
{%-       set apt_dependencies = dependencies.get('apt', none) %}
{%-       if apt_dependencies is not none %}
Ensure APT dependencies for {{ slug }} are installed:
  pkg.installed:
    - pkgs: {{ apt_dependencies|json }}
{%-       endif %}
{%-     endif %}

{{ webapp_tmp_path }}:
  file.directory:
    - makedirs: True
    - user: www-data
    - group: www-data

Ensure {{ slug }} is in deployed_projects local grain:
  grains.list_present:
    - name: deployed_projects
    - value: {{ slug }}
    - onchanges:
      - archive: Deploy {{ webapp_current_path }} package
  event.send:
    - name: projects/grains/{{ slug }}/added
    - onchanges:
      - grains: Ensure {{ slug }} is in deployed_projects local grain
    - data:
        added: "{{ slug }}"


{%-   endfor %}{#- End salt['pillar.get']('projects', {}).items() #}

{% include "projects/configure.sls" %}


{%- else %}{#- End if 'web' in salt['grains.get']('id') #}

Ensure we run web specific sls where it should:
  test.fail_without_changes:
    - name: State script not meant to be run on other node type than Web Servers

{%- endif %}{#- End Else of If 'web' in salt['grains.get']('id') #}
