include:
  - varnish

{%- set mine_internal_ips_web = salt['mine.get']('role:web', 'internal_ip_addrs', expr_form='grain') %}
{%- set list_length = mine_internal_ips_web|count() %}

{#
 #  Assuming we have at least one VM with role "web",
 #  we'll get the private IP
 #}
{%- if list_length >= 1 %}
{#
 #    Similar pattern to what's done in resolvconf state.
 #
 #    Convert as an array so we do not need to guess the
 #    name of each matching minion.
 #
 #    For example:
 #
 #        {"web1": ["172.28.128.1"], "web2": ["172.28.128.3"]}
 #
 #    Applying .items() to it would give
 #
 #        [["web1", ["172.28.128.1"]], ["web2", ["172.28.128.3"]]]
 #
 #    Where we can now extract the [0][1][0]th element; only the IP address.
 #
 #}
{%-   set first_web_node_internal_ip4 = mine_internal_ips_web.items()[0][1][0] %}
{%- endif %}

/etc/default/varnish:
  file.managed:
    - watch_in:
      - service: varnish
    - contents: |
        START=yes
        NFILES=131072
        MEMLOCK=82000
        DAEMON_OPTS="-T localhost:6082 \
                     -f /vagrant/webapps/varnish/default.vcl \
                     -s malloc,256m"

/vagrant/webapps/varnish-backend.vcl:
  file.managed:
    - watch_in:
      - service: varnish
    - contents: |
        ## Paste this at the top of your VCL
        ## cd noc/webapps
        ## git clone git@bitbucket.org:AliasWebServices/configs-varnish.git varnish
        ## Prepend the following to varnish/default.vcl
        ## Then add what's in varnish/fastly.vcl in varnish/default.vcl so we can work from there.
        backend default {
            ## Put Web Server IP address
            .host = "{{ first_web_node_internal_ip4|default('127.0.0.1') }}";
            .port = "80";
        }
        sub vcl_recv {
          set req.backend = default;
        }
