Remove stuff we dont need in Vagrant VM:
  pkg.purged:
    - pkgs:
      - puppet
      - puppet-common
      - chef
      - chef-zero

vagrant:
  user.present:
    - createhome: False
    - groups:
      - www-data
      - webapps
  group.present:
    - addusers:
      - webapps
      - vagrant
