exclude:
  # See basesystem/init.sls.
  # Because in Vagrant, we will mount those from the host
  - id: /srv/webapps/.ssh
  - id: /srv/webapps

Copy vagrant id_rsa keys:
  cmd.run:
    - creates: /srv/webapps/.ssh/id_rsa.pub
    - name: |
        mkdir -p /srv/webapps/.ssh && \
        cp /vagrant/.ssh/id_rsa /srv/webapps/.ssh/ && \
        cp /vagrant/.ssh/id_rsa.pub /srv/webapps/.ssh/ && \
        chown webapps:webapps /srv/webapps/.ssh/id_rsa /srv/webapps/.ssh/id_rsa.pub && \
        chmod 600 /srv/webapps/.ssh/id_rsa && \
        echo "changed=yes comment='SSH keys copied'"

{%- set formulas_repos = salt['pillar.get']('formulas', {}) -%}

{%- set roots_location_parent = "/etc/salt/master.d" -%}

{%- from "basesystem/macros/formulas.sls" import formulas_clone -%}

{{ formulas_clone(formulas_repos, roots_location_parent) }}
