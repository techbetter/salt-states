{%- set php_version = salt['pillar.get']('local:version_slugs:ondrej_php', '5.6') %}

include:
  - stacks.php.adminer
  ## php.debugging needs rework
  #- stacks.php.debugging

/etc/php/{{ php_version }}/mods-available/development.ini:
  file.managed:
    - contents: |
        ; Managed by Salt Stack
        error_reporting = E_ALL
        display_errors = On
        display_startup_errors = On
        log_errors = On
        error_log = /var/log/php/error.log
        log_errors_max_len = 1024
        ignore_repeated_errors = On
        ignore_repeated_source = Off
        html_errors = On
        upload_max_filesize = 35M
        post_max_size = 35M

Enable Development PHP settings and logfile:
  cmd.run:
    - name: |
        phpenmod development && \
        mkdir -p /var/log/php && \
        touch /var/log/php/error.log && \
        chown -R vagrant:vagrant /var/log/php && \
        echo "changed=yes comment='Created PHP runtime logging location in /var/log/php'"
    - stateful: True
    - unless: test -L /etc/php/{{ php_version }}/apache2/conf.d/20-development.ini

Make apache2 run as vagrant user:
  cmd.run:
    - name: |
        sed -i "s/^export APACHE_RUN_USER=www-data/export APACHE_RUN_USER=vagrant/g" /etc/apache2/envvars && \
        sed -i "s/^export APACHE_RUN_GROUP=www-data/export APACHE_RUN_GROUP=vagrant/g" /etc/apache2/envvars && \
        chown -R vagrant:vagrant /var/log/apache2
    - unless: grep -q -e "APACHE_RUN_GROUP=vagrant" /etc/apache2/envvars

Install local Memcached for development:
  pkg.installed:
    - name: memcached
  service.running:
    - name: memcached
    - reload: True
    - enable: True
