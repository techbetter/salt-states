include:
  - stacks.apache2.vhosts.enforce

/etc/update-motd.d/00-atop-header-web-dev:
  file.managed:
    - mode: 755
    - contents: |
        #!/bin/bash
        printf "\n
                                        ___.                 .___
                       __  _  __  ____  \_ |__             __| _/  ____  ___  __
                       \ \/ \/ /_/ __ \  | __ \   ______  / __ | _/ __ \ \  \/ /
                        \     / \  ___/  | \_\ \ /_____/ / /_/ | \  ___/  \   /
                         \/\_/   \___  > |___  /         \____ |  \___  >  \_/
                                     \/      \/               \/      \/\n\n
                      A Vagrant VM where you can do Web Development that's using
                      the same configuration we have in production.\n
                              http://aliaswebservices.bitbucket.org/vagrant.html\n
                      Make sure /vagrant/provision/pillar.yml is valid YAML and
                      contains all projects you want to work on in this VM.\n
                      Commands you may want to use:\n
                        - hs
                          Refresh all configuration (instead of reboot)\n
                        - salt-call state.sls projects.update
                          Pull code from projects\n
                        - sysdig -c spy_logs
                          Tail all system logs at once. See http://sysdig.org\n\n
        "
