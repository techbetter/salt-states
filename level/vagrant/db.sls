{%- set service_account = salt['pillar.get']('mysql:service_account', {}) %}

Salt Master Python MySQL bindings:
  pkg.installed:
    - name: python-mysqldb

/etc/salt/minion.d/mysql.conf:
  file.managed:
    - name: /etc/salt/minion.d/mysql.conf
    - contents: |
        mysql.default_file: '/etc/mysql/debian.cnf'

{%- if service_account.username is defined %}
{%-   if service_account.username is not sameas 'root' %}
Ensure {{ service_account.username }} database service account user exists:
  mysql_user.present:
    - host: '%'
    - name: '{{ service_account.username }}'
    - password: '{{ service_account.password }}'

{#-
 # This isn't used in AWS context, but would expect similar privileges set
 #}

Ensure '{{ service_account.username }}'@'%' database service account has access:
  mysql_grants.present:
    - grant: ALL PRIVILEGES
    - grant_option: True
    - database: '*.*'
    - host: '%'
    - user: {{ service_account.username }}
  cmd.run:
    - name: service mysql restart
{%-   endif %}
{%- endif %}
