# Infrastructure configuration code

This repository contain configuration code to manage a cluster of servers.
It assumes you've installed the code along with its pillars both shared and private into a number
**Ubuntu 14.04 LTS** VMs, with at least one VM called "noc" (a.k.a. NOC).

This system is designed to help maintain the following aspects

* Web servers
* Web Application deployment lifecycle
* Databases
* SSL certificates

## Use

Refer to documentation in [docs/](./docs/) to see how to use.

