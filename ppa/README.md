# Maintain a list of external Apt repositories

We use a few external dependencies and we want to Useful
the same repositories for all the possible configuration
variants we create.


# Useful commands

## List Apt keys

   apt-key list

# Reference

 - https://wiki.debian.org/SecureApt
