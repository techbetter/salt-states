{%- set oscodename = salt['grains.get']('oscodename') %}

/etc/apt/sources.list.d/fury_betastream.list:
  file.managed:
    - contents: 'deb https://apt.fury.io/betastream/ /'

# See https://deb.sury.org/
{%- for ppa_name in [
           'ondrej/apache2'
          ,'ondrej/php5-compat'
          ,'ondrej/php'
] %}
{%- set ppa_slug = ppa_name|replace('/','-') %}
Use {{ ppa_name }} PPA:
  pkgrepo.managed:
    - humanname: {{ ppa_name }}
    - refresh_db: True
    - dist: {{ oscodename }}
    - keyid: E5267A6C
    - keyserver: keyserver.ubuntu.com
    - name: deb http://ppa.launchpad.net/{{ ppa_name }}/ubuntu {{ oscodename }} main
    - file: /etc/apt/sources.list.d/{{ ppa_slug }}-{{ oscodename }}.list
{%- endfor %}
