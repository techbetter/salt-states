{%- set tld = salt['pillar.get']('local:tld', 'localhost.local') %}
{%- set contact_email = salt['pillar.get']('local:contact_email', 'root@' ~ tld) -%}

/etc/profile.d/mailto.sh:
  file.managed:
    - contents: "export MAILTO=\"{{ contact_email }}\""

/usr/bin/cronhelper.sh:
  file.managed:
    - source: salt://cron/files/cronhelper.sh
    - user: www-data
    - group: www-data
    - mode: 755
