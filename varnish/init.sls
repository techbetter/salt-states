include:
  - ppa

/etc/profile.d/C6_varnish_aliases.sh:
  file.managed:
    - group: users
    - source: salt://varnish/files/bash_aliases

/etc/apt/preferences.d/varnish:
  file.managed:
    - require:
      - file: /etc/apt/sources.list.d/fury_betastream.list
    - contents: |
        ## Pin Varnish version so we match Fastly's Varnish release
        ## https://wiki.debian.org/AptPreferences
        ## https://help.ubuntu.com/community/PinningHowto
        ## man apt_preferences
        Package: varnish
        Pin: version 2.1.4
        Pin-Priority: 9000

Install Varnish v2.1.4 same as Fastly for local development:
  pkg.installed:
    - name: varnish
    - version: 2.1.4
    - refresh: True
    - skip_verify: True
    - allow_updates: False
    - skip_suggestions: True
    - hold: True
    - require:
      - file: /etc/apt/sources.list.d/fury_betastream.list

varnish:
  service.running:
    - reload: True
    - enable: True
