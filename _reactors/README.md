# Salt Reactor folder

This folder has scripts we can run when events happens in Salt Even stream.

To lean more, refer to:

* [Salt event module](http://docs.saltstack.com/en/latest/ref/modules/all/salt.modules.event.html#module-salt.modules.event)
* [Salt event system](http://docs.saltstack.com/en/latest/topics/event/index.html)
* [Salt Reactor](http://docs.saltstack.com/en/latest/topics/reactor/)

See also what’s in this salt-states repository at `salt/master/files/reactors/`.
