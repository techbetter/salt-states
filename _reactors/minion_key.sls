{#
 # Upgrade packages
 #
 # This reactor isn’t run with same capabilities as a state,
 # make sure this file is refered by /etc/salt/master.d/reactors.conf
 # as a salt://_reactors/minion_key.sls.
 #
 # Data looks like:
 #
 #     {id: 'foo', act: 'accept'};
 #
 # act key can be one of: [accept,pend,reject,delete]
 #
 # `cmd.foo` and `local.foo` are aliases. In other words `cmd.pkg.upgrade`
 # is the same as `local.pkg.upgrade`.
 #
 # Source: https://github.com/webplatform/salt-states/blob/201506-refactor/reactor/reactions/new_minion.sls
 #}
{% if data['act'] == 'accept' %}
Upgrade pkgs:
  cmd.pkg.upgrade:
    - tgt: {{ data['id'] }}
    - ret: syslog

Ensure /etc/salt/minion.d/mine.conf exists:
  local.state.apply:
    - tgt: {{ data['id'] }}
    - arg:
      - salt
    - ret: syslog

Send mine data on {{ data['id'] }} minion acceptation:
  cmd.mine.send:
    - tgt: '*'
    - arg:
      - internal_ip_addrs
    - ret: syslog
{% endif %}
{% if data['act'] == 'delete' %}
Update mine data on {{ data['id'] }} minion removal:
  cmd.mine.flush:
    - tgt: '*'
    - ret: syslog

Send mine data on {{ data['id'] }} minion deletion because we flushed it:
  cmd.mine.send:
    - tgt: '*'
    - arg:
      - internal_ip_addrs
    - ret: syslog
{% endif %}
{% if data['act'] == 'reject' %}
delete_ip_addrs:
  cmd.mine.flush:
    - tgt: '*'
    - ret: syslog

Send mine data on {{ data['id'] }} minion deletion because we rejected one:
  cmd.mine.send:
    - tgt: '*'
    - arg:
      - internal_ip_addrs
    - ret: syslog
{% endif %}
