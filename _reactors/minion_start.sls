{#
 # Sync grains, modules and al.
 #
 # This reactor isn’t run as a state,
 # make sure this file is called from /etc/master.d/reactor.conf
 # as a salt://salt/reactor/reaction/foo.sls instead.
 #
 # Source: https://github.com/webplatform/salt-states/blob/201506-refactor/reactor/reactions/minion_start.sls
 #}

sync_all:
  local.saltutil.sync_all:
    - tgt: {{ data['id'] }}
    - ret: syslog
